#include "GameManager.h"
#include "Hero.h"
#include "ArealCreator.h"
#include "BackgroundManager.h"
#include "BaseStage.h"

/*GameManager::GameManager()
{
	this->init();
}
*/

GameManager::~GameManager()
{
	
}


bool GameManager::init()
{
	sceneWidth = Director::getInstance()->getVisibleSize().width;

	zoom = false;
	lt = 0;

	return true;
}

void GameManager::setup(Hero* h, ArealCreator* a)
{
	

	hero = h;
	arealCreator = a;

	beginRect = 0;
	endRect = 960 * 2;

	currentPoint = getTagetPoint();
	prePoint = (int)hero->_body->GetPosition().x;

	//schedule(CC_CALLBACK_1(GameManager::gameUpdate, this), 0.1f,"game_update");
	//schedule(schedule_selector(GameManager::gameUpdate),0.01f);
	scheduleUpdate();
}
void GameManager::update(float d)
{
	this->gameUpdate(d);
}
void GameManager::gameUpdate(float dt)
{
	
	//当前目标和当前位置的差距，慢慢减少差距
	int targetPoint = getTagetPoint();
	lt = targetPoint - currentPoint;

	if (lt < 0 && currentPoint < targetPoint+5)
	{
		currentPoint = targetPoint;
		lt = 0;
	}
	else if (lt > 0 && currentPoint > targetPoint-5)
	{
		currentPoint = targetPoint;
		lt = 0;
	}
	else
	{
		
		currentPoint = currentPoint + lt*0.1;

		//如果差距为正数，那么修正后，不该大于，反之也是
		if (lt > 0 && currentPoint > targetPoint)currentPoint = targetPoint;
		if (lt < 0 && currentPoint < targetPoint)currentPoint = targetPoint;
	}

	//log("point : %d",(int)currentPoint);
	
	//log("point : %d", (int)getTagetPoint());

	arealCreator->updatePosition(currentPoint );

}


int GameManager::getTagetPoint()
{
	hero->lockPosition = false;

	if (hero->direction)//面朝右边
	{
		float hX = hero->_body->GetPosition().x * 32.0;//英雄的x坐标
		if (hX < beginRect + sceneWidth / 3)//在靠左墙的左边3分之1内
		{
			targetPoint = beginRect;//镜头就是当前墙的起始位置
		}
		else if (hX > endRect - sceneWidth * 2 / 3)//在靠右墙的左边3分之2内
		{
			targetPoint = endRect - sceneWidth;
		}
		else
		{
			targetPoint = hX - sceneWidth / 3;

			hero->lockPosition = true;
		}
		
	}
	else
	{
		float hX = hero->_body->GetPosition().x * 32.0;
		if (hX < beginRect + sceneWidth * 2 / 3)
		{
			targetPoint = beginRect;
		}
		else if (hX > endRect - sceneWidth / 3)
		{
			targetPoint = endRect - sceneWidth;
		}
		else
		{
			targetPoint = hX - sceneWidth * 2 / 3;

			hero->lockPosition = true;
		}
		
	}

	log("%d                   %d" , hero->direction, targetPoint);


	// 左右晃动  近错位大，远错位小

	return targetPoint;
}

void GameManager::darkness()
{

	hero->stage->backgroundManager->closeLight();
	
}