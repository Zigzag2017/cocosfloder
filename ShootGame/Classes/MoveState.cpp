#include "MoveState.h"
#include "Hero.h"
#include "FiniteStateMachine.h"

void MoveState::execute(Hero *hero, MessageEnum messageEnum)
{
	//log("000000000000000000");
	switch (messageEnum)
	{
	case MessageEnum::IDLE:
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;
	case MessageEnum::MOVE:
		break;
	case MessageEnum::FILL:
		break;
	case MessageEnum::FIRE:
		if (hero->bullets <= 0)
		{
			hero->getFsm()->currentState = MessageEnum::FILL;
			hero->getFsm()->changeState();
			break;
		}
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;
	//case MessageEnum::TUMBLE:
	//	break;
	//case MessageEnum::START:
	//	break;
	case MessageEnum::SKILL:
		if (hero->lines > 0)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	}
	//hero->idle();
}

void MoveState::run(Hero *hero)
{
	hero->move();
	hero->isMove = true;
	hero->currentName = "MoveState";
}