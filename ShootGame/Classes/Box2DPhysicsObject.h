#ifndef __BOX2D_OBJECT_H__
#define __BOX2D_OBJECT_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Box2D\Box2D.h"

USING_NS_CC;

enum class ObjectClass
{
	ENEMY_OBJECT,
	HERO_OBJECT,
	BULLET_OBJECT,
	BULLET_AWAKE_OBJECT,
	ITEM_OBJECT,
	WALL_OBJECT,
	INSULATE_OBJECT,
	LINE_OBJECT
};

/*   ObjectBits
*
*	//[1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384]
*	ENEMY = 0x0002,   0000 0000 0000 0010   0000 0000 0000 1011
*	ITEM = 0x0004,	  0000 0000 0000 0100   0000 0000 0000 1101
*	INSULATE = 0x0008 0000 0000 0000 1000   0000 0000 0000 1001
*/

class Box2DPhysicsObject :
	public cocos2d::Sprite
{
public:
	virtual bool init();
	virtual bool init(const std::string& filename);

	bool initWithFrameName(const std::string& filename);

	ObjectClass objectClass;

	~Box2DPhysicsObject();

	//b2BodyDef _bodyDef;
	b2Body *_body;
	b2Shape *_shape;
	//protected var _fixtureDef : b2FixtureDef;
	b2Fixture *_fixture;
	
	b2World *_world;

	float _x;
	float _y;

	float _width;
	float _height;

	bool _beginContactCallEnabled;
	bool _endContactCallEnabled;
	bool _preContactCallEnabled;
	bool _postContactCallEnabled;
	
//	virtual void defineBody();
	virtual void createBody();
	virtual void createShape();
//	virtual void defineFixture();
	virtual void createFixture();

	//virtual void setWorld(b2World* world);

	/**
	* Override this method to handle the begin contact collision.
	*/
	virtual void handleBeginContact(b2Contact* contact);

	/**
	* Override this method to handle the end contact collision.
	*/
	virtual void handleEndContact(b2Contact* contact);

	/**
	* Override this method if you want to perform some actions before the collision (deactivate).
	*/
	virtual void handlePreSolve(b2Contact* contact, const b2Manifold* oldManifold);

	/**
	* Override this method if you want to perform some actions after the collision.
	*/
	virtual void handlePostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
	
};

#endif