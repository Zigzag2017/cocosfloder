#include "BaseBubble.h"
#include "BubbleCreator.h"
#include "GameManager.h"
#include "BaseStage.h"

USING_NS_CC;

BaseBubble::BaseBubble(int style)
{
	m_style = style;
}



/*
*层级，起点，水平速度，自转系数，重力加速度
*
*/

void BaseBubble::used(BubbleCreator *pc, int targetZOrder, float x)
{
	creator = pc;
	
	lt_c = (CCRANDOM_0_1()*3)*0.01 + 0.03;

	
	//setZOrder(targetZOrder);


	m_x = creator->baseStage->gameManager->currentPoint + x * 5 - 4 * 480;
	m_y = -320*8;

	targetX = (x + (30 - CCRANDOM_0_1()*60))  * 5 - 4 * 480;
	if (targetX - m_x > 0)moveDir = true;

	this->stage = (Node*)stage;

	m_ySpeed = (CCRANDOM_0_1()*20) / 20.0 + 1.0;
	lt = 0;

	m_scale = (CCRANDOM_0_1()*5)*0.1 + 0.7;
	this->setScale(m_scale);
	//this->setScale(20);
	log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	this->setPosition(Vec2(m_x, m_y));
	//this->setPosition(Vec2(m_x, m_y));


	


	//显示计时后，记得不让回收

	this->setVisible(true);
	scheduleUpdate();
}
void BaseBubble::putback()
{
	if (this->isVisible())
		this->setVisible(false);
	
	//*********************从池回收
	if (creator)
		creator->putback(this);
}



void BaseBubble::update(float d)
{
	//lt -= lt_c;
	lt += lt_c;
	m_ySpeed += lt;

	this->setScale(m_scale + lt*0.1);




	//if (this->getPositionY() <= m_bottom)
	//{
	//	m_ySpeed = (m_ySpeed > 0 ? m_ySpeed : -1 * m_ySpeed) / 2;//速度向上，差值为负，速度递减
	//	//lt = lt > 0 ? -1 * lt : lt;
	//	lt = 0;
	//}
	this->setRotation(this->getRotation() + 10);


	if (moveDir == true)
	{
		if(targetX > this->getPositionX()  )//	m_x = x*5-4*480;
		{
			this->setPositionX(this->getPositionX() + 3);
		}
	}
	else
	{
		if (targetX < this->getPositionX())//	m_x = x*5-4*480;
		{
			this->setPositionX(this->getPositionX() - 3);
		}
	}

	//this->setPositionX(this->getPositionX() + m_xSpeed);
	this->setPositionY(this->getPositionY() + m_ySpeed);


		if (this->getPositionY() > -1*320)
		{
			this->unscheduleUpdate();
			putback();
		}

	
}