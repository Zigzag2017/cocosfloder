#ifndef __BASE_BUBBLE_H__
#define __BASE_BUBBLE_H__

#include "cocos2d.h"
#include "BaseStage.h"

class BubbleCreator;
class BaseBubble :
	public Sprite
{
public:

	BaseBubble(int style);
	//~BaseParticle();
	static BaseBubble* create(const std::string& filename, int style)
	{
		BaseBubble *sprite = new (std::nothrow) BaseBubble(style);
		if (sprite && sprite->initWithFile(filename))
		{
			//sprite->autorelease();//记得回收
			return sprite;
		}
		CC_SAFE_DELETE(sprite);
		return nullptr;
	};

	cocos2d::Node* stage;

	float m_width;
	float m_height;

	float m_x;
	float m_y;

	float m_bottom;
	float m_xSpeed;
	float m_ySpeed;//                

	float lt;//速度差。+5和-5，一个是着地，一个是0之后
	float lt_c;//速度差增减参数，随机

	float m_rotation;

	float targetX;//X位移目标
	bool moveDir;//位移方向

	float m_scale;

	//float m_g;
	/*
	*层级，起点，水平速度，自转系数，重力加速度
	*
	*/
	BubbleCreator *creator;
	//only
	virtual void used(BubbleCreator *pc, int targetZOrder, float x);
	virtual void putback();



private:

	virtual void update(float d);


	int m_style;


};


#endif