#ifndef __FINITY_STATE_MACHINE_H  
#define __FINITY_STATE_MACHINE_H  
#include "cocos2d.h"  
#include "State.h"  

USING_NS_CC;
class Hero;
class IdleState;
class MoveState;
class StartState;
class TumbleState;
class FillState;
class FireState;
class SkillState;

class FiniteStateMachine :public Node
{
public:
	FiniteStateMachine();
	~FiniteStateMachine();
	static FiniteStateMachine*createWithHero(Hero *hero);
	bool initWithHero(Hero *hero);
	void changeState();

	void execute(MessageEnum msg);

private:
	State *mState;
	Hero *mHero;

	IdleState *idle;
	MoveState *move;
	StartState * start;
	TumbleState *tumble;
	FillState *fill;
	FireState *fire;
	SkillState *skill;

public:
	MessageEnum currentState;
	//void onRecvWantoRest(Ref*obj);
	//void onRecvWantoCoding(Ref*obj);
};
#endif