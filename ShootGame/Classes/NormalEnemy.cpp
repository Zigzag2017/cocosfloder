#include "cocos2d.h"
#include "NormalEnemy.h"
#include "BaseEnemy.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"
#include "Hero.h"
#include "GameManager.h"
#include "LineBullet.h"
#include "Box2DPhysicsObject.h"

USING_NS_CC;



NormalEnemy::NormalEnemy(float x, float y, Node* h)
{
	_x = x;
	_y = y;
	hero = (Hero*)h;
	lifes = 180;
	speed = 10000;
	isMove = false;
	isHurt = false;
	isAttack = false;
	moveAnimation = nullptr;
	attackAnimation = nullptr;
	hurtAnimation = nullptr;

	fHeight = 0.0;
	fCount = 0.0;

	isShock = false;

	isCatch = false;

	disColor = Vec4(0.0,0.0,0.0,0.0);


	
}






void NormalEnemy::handleBeginContact(b2Contact* contact)
{
	//log("contact!!!");
	Box2DPhysicsObject* other;
	b2Fixture* self;
	if (this == contact->GetFixtureA()->GetBody()->GetUserData())
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureA()->GetUserData();
	}
	else
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureB()->GetUserData();
	}
	
	if (!isCatch&&!self&&other->objectClass == ObjectClass::HERO_OBJECT)
	{
		isCatch = true;
		log("catch..........");
		if (this->isHurt == false && this->isDeath == false)
		this->attack();
	}

	if (other->objectClass == ObjectClass::BULLET_OBJECT){
		if (((MainBullet*)other)->isActive&&this->isHurt == false && this->isDeath == false)//子弹处于isActive为true才会受伤
		{
			((MainBullet*)other)->isActive = false;
			if (((MainBullet*)other)->speed > 0)//子弹速度方向，被攻击后面朝反方向
				direction = false;
			else
				direction = true;
			//this->runAction(Blink::create(0.2, 5));
			this->hurt();
		}
	}
	//log("begin...................");
}

void NormalEnemy::handleEndContact(b2Contact* contact)
{

	//**********************捕获敌人，需要重构

	//log("end...................");
	//log("contact!!!");
	Box2DPhysicsObject* other;
	b2Fixture* self;
	if (this == contact->GetFixtureA()->GetBody()->GetUserData())
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureA()->GetUserData();
	}
	else
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureB()->GetUserData();
	}

	if (!self)
	{
		isCatch = false;
		log("no catch.............");
	}
	
	if (other->objectClass == ObjectClass::LINE_OBJECT)
	{
		log("remove line~~~~~~~~~~~~~~~~~~~~~~~");
		if (hero->lineBullet->m_catchList.getIndex(this) != -1)
		{
			hero->lineBullet->m_catchList.erase(hero->lineBullet->m_catchList.getIndex(this));
			endShock();
		}

	}

}

void NormalEnemy::shock()
{
	isShock = true;
	if (!isDeath)
	bodyAction->play("hurt", true);
}
void NormalEnemy::endShock()
{
	isShock = false;
	if (!isDeath)
	bodyAction->play("idle", true);
}



bool NormalEnemy::init()
{

	if (!BaseEnemy::init())
	{
		return false;
	}

	//isMove = true;


	auto cache = AnimationCache::getInstance();
	auto frameCache = SpriteFrameCache::getInstance();
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("player/Player.plist");

	bodyNode = static_cast<cocostudio::timeline::SkeletonNode *>(CSLoader::createNode("player/Sailor.csb"));

	//bodyNode->setScale(0.8f);

	bodyAction = CSLoader::createTimeline("player/Sailor.csb");

	
	bodyNode->runAction(bodyAction);


	bodyAction->play("idle", true);


	this->addChild(bodyNode);

	//frameCache->addSpriteFramesWithFile("ePlist.plist", "ePlist.png");
	//SpriteFrame* frame = NULL;
	//Vector<SpriteFrame*> frameVec;
	//for (int i = 1; i <= 9; i++)
	//{
	//	frame = frameCache->getSpriteFrameByName(StringUtils::format("animation/enemy_attack000%d.png", i));
	//	//if (!frame)log("********************null**************************");
	//	frameVec.pushBack(frame);
	//}
	//auto attackAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, 1);
	//cache->addAnimation(attackAnimation, "attack");

	//frameVec.clear();
	//for (int i = 1; i <= 3; i++)
	//{
	//	frame = frameCache->getSpriteFrameByName(StringUtils::format("animation/enemy_hurt000%d.png", i));
	//	//if (!frame)log("********************null**************************");
	//	frameVec.pushBack(frame);
	//}
	//auto hurtAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, 1);
	//cache->addAnimation(hurtAnimation, "hurt");

	//frameVec.clear();
	//for (int i = 1; i <= 9; i++)
	//{
	//	frame = frameCache->getSpriteFrameByName(StringUtils::format("animation/enemy_move000%d.png", i));
	//	//if (!frame)log("********************null**************************");
	//	frameVec.pushBack(frame);
	//}
	//auto moveAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, -1);
	//cache->addAnimation(moveAnimation, "move");


	
	//	this->runAction(Animate::create(moveAnimation));

	//this->move();

	shadow = Sprite::createWithSpriteFrameName("P/shadow_player.png");
	shadow->setAnchorPoint(Vec2(0.5,0.5));
	//body = Sprite::create("enemy.png");
	//this->addChild(body);
	_beginContactCallEnabled = true;
	_endContactCallEnabled = true;

	scheduleUpdate();//启动定时器

	return true;
}

void NormalEnemy::update(float d)
{

	auto s = Director::getInstance()->getWinSize();

	fCount += 0.04;
	fHeight = 10 * cos(fCount);

	//m_eGLProgramState->setUniformVec2("resolution", Vec2(s.width, s.height));

	//m_eGLProgramState->setUniformVec4("v_disColor", disColor);

	//敌人状态机
	b2Vec2 position=_body->GetPosition();
	b2Vec2 heroPosition = hero->_body->GetPosition();

	
	if (position.x - heroPosition.x > 0)
	{
		this->setPositionX(position.x * 32.0 - 10. - this->stage->gameManager->currentPoint);
		this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight);
	}
	else
	{
		this->setPositionX(position.x * 32.0 + 10. - this->stage->gameManager->currentPoint);
		this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight);
	}
	


	shadow->setPositionX(_body->GetPosition().x * 32.0 - this->stage->gameManager->currentPoint);
	shadow->setPositionY(_body->GetPosition().y * 5.536 + 150);
	//shadow->setScaleY(0.3);

	this->setZOrder(1000 - (int)position.y);

	if (isShock)
	{
		this->lifes -= 1;
		if (this->lifes <= 0)
		{
			death();
			isShock = false;
		}
	}
	
	
	if (isHurt || isDeath){
		
	}
	else if (isAttack){
		//this->attack();
	}
	else{
		this->move();
		b2Vec2 v;
		if (position.x - heroPosition.x > 0)
		{
			v.x = -1 * speed;
			direction = true;
			this->setScaleX(1.2);
			this->setScaleY(1.2);
			_body->SetTransform(position, 3.14);

		}
		else
		{
			v.x = speed;
			direction = false;
			this->setScaleX(-1.2);
			this->setScaleY(1.2);
			_body->SetTransform(position, 0);

		}
		if (position.y - heroPosition.y > 0)
		{
			v.y = -0.3 * speed;
		}
		else
		{
			v.y = 0.3 * speed;
		}

		//_body->SetLinearVelocity(v);
		_body->ApplyLinearImpulse(v, _body->GetWorldCenter(), true);

	}

}


void NormalEnemy::idle()
{

}

void NormalEnemy::move()
{
	if (!isMove)
	{
		bodyAction->clearLastFrameCallFunc();
		this->stopActionByTag(222);

		isMove = true;
		//moveAnimation = AnimationCache::getInstance()->getAnimation("move");
		bodyAction->play("walk", true);
		//auto action = Animate::create(moveAnimation);
	//	auto action = RepeatForever::create(a);
		//action->setTag(222);//同一个状态机
		//this->runAction(action);
	}
}
void NormalEnemy::attack()
{
	if (!this->isAttack)
	{
		bodyAction->clearLastFrameCallFunc();
		this->stopActionByTag(222);
		this->isAttack = true;
		isHurt = false;
		isMove = false;

		bodyAction->play("attack", false);
		bodyAction->setLastFrameCallFunc(
			CC_CALLBACK_0(NormalEnemy::endAttack, this)
			//[this](){
			//this->bodyAction->clearLastFrameCallFunc();
			//isLine = true;
	//	}
	);

		//attackAnimation = AnimationCache::getInstance()->getAnimation("attack");
		//auto a = Animate::create(attackAnimation);
		//
		//auto seq = Sequence::create(a, CallFunc::create(CC_CALLBACK_0(NormalEnemy::endAttack, this)), NULL);
		//seq->setTag(222);//同一个状态机
		//this->runAction(seq);

		
		//seq->setTag(100);//受伤无法被中断，但是攻击会，给攻击用
		//this->runAction(seq);
	}
}
void NormalEnemy::checkAttack()
{
	if (isCatch == true)
	{
		hero->hurt();
	}
}

void NormalEnemy::endAttack()
{

	this->bodyAction->clearLastFrameCallFunc();

	//没有攻击帧的简单判断
	checkAttack();

	this->isAttack = false;
	//isMove = true;
	if(isCatch == true)
	if (this->isHurt == false && this->isDeath == false)
	{
		this->attack();
	}
}

void NormalEnemy::hurt(){
	if (!this->isHurt&&!this->isDeath)
	{
		bodyAction->clearLastFrameCallFunc();
		//根据受伤方向，确定火星溅射方向
		if (!this->direction)
			stage->particleCreator->makeParticles(this->getZOrder(), shadow->getPositionX(), shadow->getPositionY() + 100, shadow->getPositionY() + 30, 1);
		else
			stage->particleCreator->makeParticles(this->getZOrder(), shadow->getPositionX(), shadow->getPositionY() + 100, shadow->getPositionY() + 30, -1);

		this->lifes -= 50;//扣血，确定子弹，扣多少//************

		this->stopActionByTag(222);//受伤停止攻击
		this->isAttack = false;
		this->isHurt = true;
		isMove = false;
		//hurtAnimation = AnimationCache::getInstance()->getAnimation("hurt");
		//auto a = Animate::create(hurtAnimation);
		bodyAction->play("hurt", false);
		auto seq = Sequence::create(DelayTime::create(0.2), CallFunc::create(CC_CALLBACK_0(NormalEnemy::endHurt, this)), NULL);
		seq->setTag(222);//同一个状态机
		this->runAction(seq);

		//this->setColor(Color3B::RED);
		
	}
}
void NormalEnemy::endHurt(){
	bodyAction->clearLastFrameCallFunc();
	//this->setColor(Color3B::WHITE);

	if (this->lifes < 0)
	{
		death();
	}
	this->isHurt = false;
	//this->isMove = true;
	//this->setColor(Color3B::);
}

void NormalEnemy::death()
{

	bodyAction->clearLastFrameCallFunc();

	this->isDeath = true;

	if (hero->lineBullet->m_catchList.getIndex(this) != -1)
	{
		hero->lineBullet->m_catchList.erase(hero->lineBullet->m_catchList.getIndex(this));
	}

	//this->setColor(Color3B::RED);
	//hurtAnimation = AnimationCache::getInstance()->getAnimation("hurt");
	//auto a = Animate::create(hurtAnimation);
	bodyAction->play("explosion", false);

	bodyAction->setLastFrameCallFunc(
		CC_CALLBACK_0(NormalEnemy::endDeath, this)
		);

	//auto seq = Sequence::create(a, CallFunc::create(CC_CALLBACK_0(NormalEnemy::endDeath, this)), NULL);
	//this->runAction(seq);
}

void NormalEnemy::endDeath()
{

	bodyAction->clearLastFrameCallFunc();

	stage->killNum++;

	stage->shadowLayer->removeChild(shadow);
	stage->objectLayer->removeChild(this);
	
}

void NormalEnemy::turning(){}

void NormalEnemy::setStage(BaseStage *s)
{
	stage = s;
	s->shadowLayer->addChild(shadow);
}