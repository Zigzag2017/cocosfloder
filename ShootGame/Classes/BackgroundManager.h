#ifndef __BACKGROUND_MANAGER_H__
#define __BACKGROUND_MANAGER_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

class Hero;
class BaseStage;
class BubbleCreator;

class BackgroundManager : public cocos2d::Node
{
public:

	~BackgroundManager();

	CREATE_FUNC(BackgroundManager);

	virtual bool init();

	void makeBubble(Node* node);

	void closeLight();//�ڰ����٣��رղ���

	Hero* hero;
	BaseStage* stage;

	cocos2d::Sprite* background;

	cocos2d::Sprite* flowerLayer;

	cocos2d::Sprite* floorLayer;


	cocos2d::Sprite3D* skyA;

	cocos2d::Sprite* flowerA;//��Ƭ���㣬�滻
	cocos2d::Sprite* flowerB;

	cocos2d::Sprite* stoneA;//��Ƭʯͷ�㣬�滻
	cocos2d::Sprite* stoneB;

	cocos2d::Sprite* bubbleLayer;
	BubbleCreator* bubbleCreator;
	//particleCreator = new ParticleCreator(8, 1);
	//particleCreator->setStage(this);

	cocos2d::CCParticleSystemQuad* m_particleSystem;

	//cocos2d::Sprite3D* skyB;
	cocos2d::Sprite3D* floorA;
	cocos2d::Sprite3D* floorB;

	cocos2d::GLProgramState * m_pGLProgramState;

	cocos2d::GLProgramState * m_flowerGLProgramState;

	//uv����ֵ
	cocos2d::Vec2 m_LightAni;

	void setup(Hero* h,BaseStage* s);

	void update(float delta);

	void updatePosition(float l);
};

#endif // __BACKGROUND_MANAGER_H__