#include "LineBullet.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"
#include "GameManager.h"
#include "Hero.h"
#include "NormalEnemy.h"

USING_NS_CC;

//****隐藏时候逻辑停止，粒子停止

LineBullet::LineBullet(float x, float y, int type)
{
	_x = x;
	_y = y;
	isSetup = false;
	log("run LineBullet -> yes");
	particleIndex = 0;

	auto s = Director::getInstance()->getWinSize();

	particleLayer = Sprite::create();
	this->addChild(particleLayer);

	m_particleSystem = CCParticleSystemQuad::create("particle/lightning.plist");
	//pParticleSystem->setGravity(Vec2(400,-800));
	m_particleSystem->setTotalParticles(500);
	m_particleSystem->setPosition(Vec2(s.width / 2, s.height / 2));
	//m_particleSystem->stopSystem();
	m_particleSystem->retain();
	m_particleSystem->setAutoRemoveOnFinish(true);
	particleLayer->addChild(m_particleSystem);


	lightning = ShaderNode::shaderNodeWithVertex("shader/lightning.vsh", "shader/lightning.fsh");

	lightning->setPosition(Vec2(s.width / 2, s.height / 2));
	addChild(lightning);

	


	this->setVisible(false);
}

void LineBullet::createFixture()
{
	log("LineBullet createFixture");

	//bodyDef.linearDamping = 1;

	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.1;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;
	fixtureDef.userData = this;//身体储存对象，hit没有东西
	fixtureDef.isSensor = true;
	//fixtureDef.filter.categoryBits = 0x0004;//ObjectBits::ITEM;
	//fixtureDef.filter.maskBits = 0x0015;
	//fixtureDef.filter.groupIndex = 1;
	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);

}

void LineBullet::createShape()
{
	log("LineBullet createShape");


	b2PolygonShape *shape = new b2PolygonShape();
	shape->SetAsBox(960 / 2 / 32.0, 40 / 2 / 32.0);
	_shape = shape;

}

void LineBullet::createBody()
{
	log("LineBullet createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(_x / 32.0, _y / 32.0);
	bodyDef.fixedRotation = true;
	bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
	//_body->SetAwake(false);
	_body->SetActive(false);
}


void LineBullet::start()
{
	isSetup = true;
	//
	stage->hero->isLine = true;

	_body->SetActive(true);

	this->setVisible(true);
}

void LineBullet::stop()
{
	isSetup = false;
	//_body->SetAwake(false);
	stage->hero->isLine = false;

	_body->SetActive(false);

	this->setVisible(false);
}

void LineBullet::handleBeginContact(b2Contact* contact)
{
	log("LineBullet and enemy contact!!!");
	Box2DPhysicsObject* other = (this == contact->GetFixtureA()->GetBody()->GetUserData()) ? (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData() : (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
	if (other->objectClass == ObjectClass::ENEMY_OBJECT)
	{
		log("find enemy~~~~~~~~~~~~~~~~~~~~~~~");

		bool flag = false;
		for (int i = 0; i < m_catchList.size(); ++i)
		{
			if (m_catchList.at(i) == other)
			{
				flag = true;
			}
		}
		if (!flag&&!stage->flag1)
		{
			NormalEnemy* e = (NormalEnemy*)other;
			e->shock();
			m_catchList.pushBack(other);;
		}
	}

}



LineBullet::~LineBullet()
{
	log("delete LineBullet");
	_world->DestroyBody(_body);
}

void LineBullet::handleEndContact(b2Contact* contact)
{
	//log("contact!!!");
}


bool LineBullet::init(int type)//道具类型~~~~~~~~~~~~~~~~~~~~~~
{
	//std::string filename = "item.png";
	if (!Box2DPhysicsObject::init())//filename))
	{
		return false;
	}

	objectClass = ObjectClass::LINE_OBJECT;

	scheduleUpdate();//启动定时器

	return true;
}

//static bool  LineBullet::sort_by_distance(const Box2DPhysicsObject& obj1, const Box2DPhysicsObject& obj2)
//{
//	return obj1.getPositionX() > obj2.getPositionX();
//}

void LineBullet::update(float d)//************绘制函数启动在用
{

	//lightning->setHeroPosition(Vec2(0.0,1.0),true);

	b2Vec2 hPosition = stage->_hero->GetPosition();

	if (isSetup)
	{
		if (stage->hero->direction)
			_body->SetTransform(b2Vec2(hPosition.x + 480 / 32.0, hPosition.y), (float32)0.0);
		else
			_body->SetTransform(b2Vec2(hPosition.x - 480 / 32.0, hPosition.y), (float32)0.0);
	}


	b2Vec2 position = _body->GetPosition();
	auto s = Director::getInstance()->getWinSize();
	if (stage->hero->direction)
	{
		lightning->setHeroPosition(Vec2((stage->hero->getPositionX() + 125) * 2 / s.width, (stage->hero->getPositionY() + 25) * 2 / s.height ), stage->hero->direction);
	}
	else
	{
		lightning->setHeroPosition(Vec2((stage->hero->getPositionX() - 125) * 2 / s.width, (stage->hero->getPositionY() + 25) * 2 / s.height), stage->hero->direction);
	}
	//this->setPositionX(position.x * 32.0 - this->stage->gameManager->currentPoint);
	//this->setPositionY(position.y * 16.0 + 40);

	

	//this->setZOrder(1000 - (int)position.y);
	//ZOrder->没击中敌人,使用英雄深度，有击中敌人，使用最表面敌人的深度

	if (m_catchList.empty())//是否为空，为空没击中敌人
	{
		this->setZOrder(stage->hero->getZOrder());
		m_particleSystem->setVisible(false);
		//m_particleSystem->setTotalParticles(0);
		//m_particleSystem->stopSystem();
		
		m_particleSystem->setPositionY(-100);
	}
	else
	{
		this->setZOrder(stage->hero->getZOrder() + 48);

		

		//存在敌人，确认粒子数量，方向
		//int particleNum = 0;
		//if(m_catchList.size() < 10)
		//	particleNum = m_catchList.size() * 50;
		//else
		//	particleNum = 500;

		//m_particleSystem->setTotalParticles(particleNum);

		if (stage->hero->direction)
			m_particleSystem->setGravity(Vec2(-400,-800));
		else
			m_particleSystem->setGravity(Vec2(400, -800));

		//m_particleSystem->resetSystem();
		particleIndex++;
		if (particleIndex >= m_catchList.size())particleIndex = 0;
		m_particleSystem->setPosition(Vec2(m_catchList.at(particleIndex)->getPositionX(), m_catchList.at(particleIndex)->getPositionY()+30));
		m_particleSystem->setVisible(true);
	}

	//for (int i = 0; i < m_catchList.size(); ++i)
	//{
	//	Node *n = (Node *)m_catchList.at(i);
	//	log("front  %f", n->getPositionX());

	//}

	m_electricList.clear();//清空数组，电光列表

	//m_electricList.push_back(Vec2(hPosition.x, hPosition.y));
	

	//std::sort(m_catchList.begin(), m_catchList.end(), LineBullet::sort_by_distance);//排序函数，从左往右

	//for (int i = 0; i < m_catchList.size(); ++i)
	//{
	//	Node *n = (Node *)m_catchList.at(i);
	//	log("back  %f", n->getPositionX());

	//	m_electricList.push_back(Vec2(n->getPositionX(), n->getPositionY()));

	//}
	
	if (m_catchList.size())
	{
		
	}
	//if (stage->hero->direction)
	//	m_electricList.push_back(Vec2(hPosition.x + 960, hPosition.y));
	//else
	//	m_electricList.push_back(Vec2(hPosition.x - 960, hPosition.y));

	std::vector<Vec2>::iterator iter;
	for (iter = m_electricList.begin(); iter != m_electricList.end(); iter++)
	{
		Vec2 n = (Vec2)*iter;
		log("position  %f  %f", n.x, n.y);
	}

	//log("catch num %d ~~~~~~~~~~~~~~~",m_catchList.size());

}


void LineBullet::drawElectricity()
{

}


void LineBullet::setStage(BaseStage* s)
{
	this->stage = s;
	
}



//lightning shader


ShaderNode::ShaderNode()
:_center(Vec2(0.0f, 0.0f))
, _resolution(Vec2(0.0f, 0.0f))
, _time(0.0f)
{
	_rect_right_up = Director::getInstance()->getWinSize();
	_rect_left_down = Vec2(0.0, 0.0);
}

ShaderNode::~ShaderNode()
{
}

ShaderNode* ShaderNode::shaderNodeWithVertex(const std::string &vert, const std::string& frag)
{
	auto node = new (std::nothrow) ShaderNode();
	node->initWithVertex(vert, frag);
	node->autorelease();

	return node;
}

bool ShaderNode::initWithVertex(const std::string &vert, const std::string &frag)
{
	_vertFileName = vert;
	_fragFileName = frag;
#if CC_ENABLE_CACHE_TEXTURE_DATA
	auto listener = EventListenerCustom::create(EVENT_RENDERER_RECREATED, [this](EventCustom* event){
		this->setGLProgramState(nullptr);
		loadShaderVertex(_vertFileName, _fragFileName);
	});

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
#endif

	loadShaderVertex(vert, frag);

	_time = 0;

	auto s = Director::getInstance()->getWinSize();

	_resolution = Vec2(s.width, s.height);
	scheduleUpdate();

	setContentSize(Size(s.width, s.height));
	setAnchorPoint(Vec2(0.5f, 0.5f));


	return true;
}

void ShaderNode::loadShaderVertex(const std::string &vert, const std::string &frag)
{
	auto fileUtiles = FileUtils::getInstance();

	// frag
	auto fragmentFilePath = fileUtiles->fullPathForFilename(frag);
	auto fragSource = fileUtiles->getStringFromFile(fragmentFilePath);

	// vert
	std::string vertSource;
	if (vert.empty()) {
		vertSource = ccPositionTextureColor_vert;
	}
	else {
		std::string vertexFilePath = fileUtiles->fullPathForFilename(vert);
		vertSource = fileUtiles->getStringFromFile(vertexFilePath);
	}

	auto glprogram = GLProgram::createWithByteArrays(vertSource.c_str(), fragSource.c_str());
	auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
	setGLProgramState(glprogramstate);
}

void ShaderNode::update(float dt)
{
	// _time += dt;

}

void ShaderNode::setPosition(const Vec2 &newPosition)
{
	Node::setPosition(newPosition);
	auto position = getPosition();
	auto frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto retinaFactor = Director::getInstance()->getOpenGLView()->getRetinaFactor();
	_center = Vec2(position.x * frameSize.width / visibleSize.width * retinaFactor, position.y * frameSize.height / visibleSize.height * retinaFactor);
}

//uv.x+0.5;右边0.5平稳  uv.x+1.5;右边1.5平稳  uv.x-0.5;左边0.5平稳
//uv.y+0.5;向下
void ShaderNode::setHeroPosition(const cocos2d::Vec2 &newPosition, bool direction)
{
	if (direction)//朝向右边
	{
		_center.x = -1 * newPosition.x;
		_center.y = 1 - newPosition.y;
		_rect_right_up = Director::getInstance()->getWinSize();
		_rect_left_down = Vec2(Director::getInstance()->getWinSize().width * newPosition.x * 0.5, 0.0);
	}
	else
	{
		_center.x = 2 - newPosition.x;
		_center.y = 1 - newPosition.y;
		_rect_right_up = Vec2(Director::getInstance()->getWinSize().width * newPosition.x * 0.5, Director::getInstance()->getWinSize().height);
		_rect_left_down = Vec2(0.0, 0.0);

	}

	//_center = Vec2(0.0, 0.0);
}

void ShaderNode::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	_customCommand.init(_globalZOrder, transform, flags);
	_customCommand.func = CC_CALLBACK_0(ShaderNode::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);
}

void ShaderNode::onDraw(const Mat4 &transform, uint32_t flags)
{


	GLfloat vertices[12] = { _rect_left_down.x, 0, _rect_right_up.x, 0, _rect_right_up.x, _rect_right_up.y,
		_rect_left_down.x, 0, _rect_left_down.x, _rect_right_up.y, _rect_right_up.x, _rect_right_up.y };

	auto glProgramState = getGLProgramState();
	glProgramState->setUniformVec2("resolution", _resolution);
	glProgramState->setUniformVec2("center", _center);
	glProgramState->setVertexAttribPointer("a_position", 2, GL_FLOAT, GL_FALSE, 0, vertices);

	glProgramState->apply(transform);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	CC_INCREMENT_GL_DRAWN_BATCHES_AND_VERTICES(1, 6);
}

