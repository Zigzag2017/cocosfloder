#ifndef __BULLET_CREATOR_H__
#define __BULLET_CREATOR_H__

#include "MainBullet.h"
#include "cocos2d.h"

using namespace cocos2d;

class BulletCreator
{
public:
	BulletCreator(int initSize,int style);
	~BulletCreator();
	MainBullet* request();
	void putback(MainBullet* bullet);
	void garbage();
private:
	void allocate(int size);
	static void freeBullet(void *bullet);
	Vector<MainBullet*> m_freeList;
	Vector<MainBullet*> m_usedList;

	int m_initSize;
	int m_style;

};



#endif