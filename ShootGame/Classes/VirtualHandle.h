#ifndef __VIRTUALHANDLE_H__
#define __VIRTUALHANDLE_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;


enum class VirtualHandleEvent
{
	TOUCH_MOVE,
	END_MOVE,
	LEFT_ATTACK_BTN_DWON,
	LEFT_ATTACK_BTN_UP,
	RIGHT_ATTACK_BTN_DWON,
	RIGHT_ATTACK_BTN_UP,
	BOTTOM_ATTACK_BTN_DWON,
	BOTTOM_ATTACK_BTN_UP
};

class VirtualHandle :public Node
{
protected:
	/**
	* 摇杆底
	*/
	ui::Button* _rockerRange;
	/**
	* 摇杆球
	*/
	cocos2d::Sprite* _rocker;
	ui::Button* _right_attack;
	ui::Button* _left_attack;
	ui::Button* _bottom_attack;

	int _rockerRangeValue;

	int _rockerTouchID;

	int _rockerWay;//0无，1左。2右

	int _rockerLastPointX;//用户记录x坐标
	int _rockerLastPointY;
	// 回调函数
	std::function<void(VirtualHandleEvent, float)> _callback;

	void touchEvent(Ref *obj, ui::Widget::TouchEventType type);

	

	inline void callback(VirtualHandleEvent event, float data);

	inline void updateRockerPos(Vec2 position);
	inline void cancelWay();
public:
	VirtualHandle();
	virtual ~VirtualHandle();
	CREATE_FUNC(VirtualHandle);
	virtual bool init();

	// 设置回调回调函数
	void setCallback(std::function<void(VirtualHandleEvent, float)> callback);
	void setFireEvent(ui::Button *left, ui::Button *right);
};


#endif