#ifndef __BASE_STAGE_H__
#define __BASE_STAGE_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "GLES-Render.h"
#include "Box2DContactListener.h"
#include "ArealCreator.h"
#include "BulletCreator.h"
#include "ShadowLayer.h"
#include "EnemyCreator.h"
#include "ItemCreator.h"
#include "ParticleCreator.h"
#include "LightLayer.h"
#include "DarkLayer.h"


class Hero;
class GameManager;
class BackgroundManager;
class LineBullet;

class BaseStage : public cocos2d::Layer
{
public:
	Label* l;
	Sprite* floor;
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();



	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	virtual void update(float dt);

	// implement the "static create()" method manually
	CREATE_FUNC(BaseStage);

	void InitUI();
	void onButtonClick(cocos2d::Ref* sender);

	void test(cocos2d::Ref* sender);

	cocos2d::ui::Button* leftBtn;
	cocos2d::ui::Button* rightBtn;

	b2World* _world;

	b2Body* _hero;
	Hero* hero;

	ShadowLayer* shadowLayer;//影子
	Layer* objectLayer;

	//DarkLayer* darkLayer;

	Sprite* darkPic;
	Sprite* d;

	LightLayer* lightLayer;//海底阳光射线层

	ArealCreator* arealCreator;//创建边框区域

	EnemyCreator* enemyCreator;//创建敌人，并管理

	ItemCreator* itemCreator;//创建道具，并管理

	BulletCreator* bulletCreator;//创建子弹池，维护管理子弹
	LineBullet *lineBullet;

	ParticleCreator* particleCreator;

	GameManager* gameManager;//镜头
	BackgroundManager* backgroundManager;//背景

	//Box2DContactListener* contactListener;

	virtual b2Body* createObjet(float posX, float posY, float radius);

	virtual b2Body* createBox(float posX, float posY, float width, float height);

	GLESDebugDraw *_debugDraw;

	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags);



	/*临时堆放区域*/
	int killNum;
	bool btn1;
	bool btn2;
	bool btn3;
	bool btn4;

	bool flag1;//打开箱子
	bool flag2;//移除箱子
	

};

#endif // __HELLOWORLD_SCENE_H__