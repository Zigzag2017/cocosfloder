#ifndef __WELCOME_READER_H__
#define __WELCOME_READER_H__

#include "cocos2d.h"
#include "cocostudio\CocosStudioExport.h"
#include "cocostudio\WidgetReader\NodeReader\NodeReader.h"
#include "Welcome.h"

class WelcomeReader : public cocostudio::NodeReader
{
public:
	WelcomeReader(){};
	~WelcomeReader(){};
	static WelcomeReader* getInstance();
	static void purge();
	cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions);
};

#endif