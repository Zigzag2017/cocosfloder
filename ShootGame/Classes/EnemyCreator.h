#ifndef __ENEMY_CREATOR_H__
#define __ENEMY_CREATOR_H__

#include <stdio.h>
#include "cocos2d.h"
#include "Box2DPhysicsObject.h"
#include "BaseEnemy.h"

USING_NS_CC;


class EnemyCreator
{
public:
	EnemyCreator();
	~EnemyCreator();
	void addNormalEnemy(Node* mainView, float x, float y);
	void addBoss(Node* mainView, float x, float y);
};

#endif