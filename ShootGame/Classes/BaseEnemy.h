#ifndef __BASE_ENEMY_H__
#define __BASE_ENEMY_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Box2DPhysicsObject.h"


USING_NS_CC;

class BaseStage;

class BaseEnemy :
	public Box2DPhysicsObject
{
public:

	BaseEnemy();
	virtual bool init();
	//CREATE_FUNC(BaseEnemy);
	~BaseEnemy();
	


	virtual void createFixture();
	virtual void createShape();
	virtual void createBody();

	virtual void handleBeginContact(b2Contact* contact);
	virtual void handleEndContact(b2Contact* contact);

	/////////////////身体状态
	virtual void idle();//站立，待定

	virtual void move();//移动
	virtual void attack();//攻击
	virtual void checkAttack();
	virtual void endAttack();//攻击
	virtual void hurt();//受伤
	virtual void endHurt();//受伤

	virtual void death();//死亡
	virtual void endDeath();//结束死亡

	virtual void turning();//转身

	int lifes;//生命值

	bool direction;//方向 right-true
	bool isMove;
	bool isAttack;
	bool isHurt;
	bool isDeath;

	int speed;//移动速度

	b2Shape *_hitShape;
	b2Fixture *_hitFixture;

	BaseStage* stage;

	Sprite* shadow;
	//Sprite* body;

private:

	virtual void update(float d);

	int identifier;//唯一标识符
	
	float moveAngle;//移动角度

	
	


};


#endif