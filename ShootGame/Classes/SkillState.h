#ifndef __SKILL_STATE_H_  
#define __SKILL_STATE_H_  
#include "State.h"  
#include "cocos2d.h"

USING_NS_CC;
class SkillState :public State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum);
	virtual void run(Hero *hero);
	
	void endSkill(Node *node);

};

#endif  