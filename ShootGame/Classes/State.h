#ifndef __STATE__H_
#define __STATE__H_


enum class MessageEnum
{
	IDLE,
	MOVE,
	FILL,
	START,
	FIRE,
	TUMBLE,
	SKILL
};

class Hero;
class State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum) = 0;
	virtual void run(Hero *hero) = 0;

};

#endif