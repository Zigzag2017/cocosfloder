#include "ItemCreator.h"
#include "BaseItem.h"
#include "BaseStage.h"

ItemCreator::ItemCreator()
{

}


ItemCreator::~ItemCreator()
{

}

void ItemCreator::addItem(Node* mainView, float x, float y,int type)
{
	BaseStage *stage = (BaseStage*)mainView;

	Node* hero = (Node*)stage->_hero->GetUserData();

	auto item = BaseItem::create(x, y, type);
	item->setStage((BaseStage*)mainView);
	stage->objectLayer->addChild(item);
}

