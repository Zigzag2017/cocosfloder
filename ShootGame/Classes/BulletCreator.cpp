#include "BulletCreator.h"
#include "ui/CocosGUI.h"
#include <math.h>

USING_NS_CC;

BulletCreator::BulletCreator(int initSize,int style)
{
	m_initSize = initSize;
	allocate(initSize);
}

BulletCreator::~BulletCreator()
{
	

}

MainBullet* BulletCreator::request()
{
	if (m_freeList.size() == 0)
	{
		allocate(m_initSize / 2);
	}

	MainBullet* bullet = *m_freeList.begin();

	m_freeList.erase(m_freeList.begin());
	m_usedList.pushBack(bullet);
	log("0.0");
	log("free %d  used %d",m_freeList.size(),m_usedList.size());

	return bullet;
}

void BulletCreator::putback(MainBullet* bullet)
{
	for (int i = 0; i < m_usedList.size();++i)
	{
		if (m_usedList.at(i) == bullet)
		{
			m_usedList.erase(m_usedList.begin()+i);
			m_freeList.pushBack(bullet);
		}
	}
}

/**
*	回收多余长度
*/
void BulletCreator::garbage()
{
	if (m_freeList.size() > m_initSize)
	{
		Vector<MainBullet*>::iterator it = m_freeList.begin();
		int pIndex = 0;

		while (it != m_freeList.end())
		{
			if (pIndex > m_initSize)
			{
				MainBullet* b = (*it);
				m_freeList.erase(it);
				freeBullet(b);
				--it;
			}
			++pIndex;
			++it;
		}
	}
}

void BulletCreator::allocate(int size)
{
	for (int i = 0; i < size; ++i)
	{
		MainBullet* b = new MainBullet(m_style);
		m_freeList.pushBack(b);
	}
}

void BulletCreator::freeBullet(void *bullet)
{
	log(" after delete ");


	MainBullet* b = (MainBullet*)bullet;
	delete b;
}