#include "FiniteStateMachine.h"  
#include "Hero.h"  
#include "State.h"  
#include "IdleState.h"
#include "MoveState.h"
//#include "StartState.h"
//#include "TumbleState.h"
#include "FillState.h"
#include "FireState.h"
#include "SkillState.h"
#define NOTIFY NotificationCenter::getInstance()


FiniteStateMachine::~FiniteStateMachine() {
	//不要在这里减fsm计数
	//NOTIFY->removeAllObservers(this);
	CC_SAFE_DELETE(mState);

	CC_SAFE_DELETE(idle);
	CC_SAFE_DELETE(move);
	CC_SAFE_DELETE(start);
	CC_SAFE_DELETE(tumble);
	CC_SAFE_DELETE(fill);
	CC_SAFE_DELETE(skill);
}

FiniteStateMachine::FiniteStateMachine() :
mState(nullptr),
mHero(nullptr)
{
	idle = new IdleState();
	move = new MoveState();
//	start = new StartState();
//	tumble = new TumbleState();
	fill = new FillState();
	fire = new FireState();
	skill = new SkillState();
}

FiniteStateMachine* FiniteStateMachine::createWithHero(Hero *hero) {


	FiniteStateMachine *fsm = new FiniteStateMachine();

	if (fsm&&fsm->initWithHero(hero)) {
		fsm->autorelease();
	}
	else {
		CC_SAFE_DELETE(fsm);
		fsm = NULL;
	}
	return fsm;
}

bool FiniteStateMachine::initWithHero(Hero *hero) {

	mState = nullptr;
	mHero = hero;
	//mHero->retain();

	/*NOTIFY->addObserver(
	this,
	callfuncO_selector(FiniteStateMachine::onRecvWantoCoding),
	StringUtils::toString(EN_WantToCoding),
	NULL);

	NOTIFY->addObserver(
	this,
	callfuncO_selector(FiniteStateMachine::onRecvWantoRest),
	StringUtils::toString(EN_WantToRest),
	NULL);*/
	return true;
}

void FiniteStateMachine::changeState() {

	switch (currentState)
	{
	case MessageEnum::FILL:
		mState = fill;
		break;
	case MessageEnum::FIRE:
		mState = fire;
		break;
	case MessageEnum::IDLE:
		mState = idle;
		break;
	case MessageEnum::MOVE:
		mState = move;
		break;
	case MessageEnum::SKILL:
		mState = skill;
		break;
	//case MessageEnum::START:
	//	mState = start;
	//	break;
	//case MessageEnum::TUMBLE:
	//	mState = tumble;
	//	break;

	}

	mState->run(mHero);

	//mState->execuete(mHero, MessageEnum::FIRE);
}

void FiniteStateMachine::execute(MessageEnum msg) {


	mState->execute(mHero, msg);//响应

}

/*
void FiniteStateMachine::onRecvWantoRest(Ref*obj) {
if (mState == NULL || mGameObj == NULL) {
return;
}
mState->execuete(mGameObj, EN_WantToRest);
}

void FiniteStateMachine::onRecvWantoCoding(Ref*obj) {
if (mState == NULL || mGameObj == NULL) {
return;
}
mState->execuete(mGameObj, EN_WantToCoding);
}*/