#include "BaseEnemy.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"

USING_NS_CC;



BaseEnemy::BaseEnemy()
{
	lifes = 100;
	speed = 0;
	moveAngle = 0;
	direction = true;
	isMove = false;
	isAttack = false;
	isHurt = false;
	isDeath = false;
	log("run BaseEnemy -> yes");

}

BaseEnemy::~BaseEnemy()
{
	log("delete enemy");
	_world->DestroyBody(_body);
}

void BaseEnemy::createFixture()
{
	log("BaseEnemy createFixture");

	//bodyDef.linearDamping = 1;



	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.1;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;
	fixtureDef.userData = this;//身体储存对象，hit没有东西
	fixtureDef.filter.categoryBits = 0x0002;//ObjectBits::ENEMY;
	fixtureDef.filter.maskBits = 0x0013;
	//fixtureDef.filter.groupIndex = 1;
	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);

	b2FixtureDef hitFixtureDef;
	hitFixtureDef.density = 1000;
	hitFixtureDef.friction = 0.3;
	hitFixtureDef.restitution = 0;
	hitFixtureDef.shape = _hitShape;
	hitFixtureDef.isSensor = true;

	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&hitFixtureDef);
}

void BaseEnemy::createShape()
{
	log("BaseEnemy createShape");

	b2CircleShape *shape = new b2CircleShape();
	shape->m_radius = 48.0f / 32.0f;
	_shape = shape;

	b2PolygonShape *rectShape = new b2PolygonShape();
	b2Vec2 v[4];
	v[0].Set(96/32.0, -48 / 32.0);
	v[1].Set(96/32.0, 48 / 32.0);
	v[2].Set(0, 48 / 32.0);
	v[3].Set(0, -48 / 32.0);
	rectShape->Set(v,4);
	//rectShape->SetAsBox(48 / 2 / 32.0, 96 / 2 / 32.0);
	_hitShape = rectShape;


}

void BaseEnemy::createBody()
{
	log("BaseEnemy createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(_x / 32.0, _y / 32.0);
	bodyDef.fixedRotation = true;
	bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
}



void BaseEnemy::handleBeginContact(b2Contact* contact)
{
	log("contact!!!");
}

void BaseEnemy::handleEndContact(b2Contact* contact)
{
	//log("contact!!!");
}


bool BaseEnemy::init()
{

	if (!Box2DPhysicsObject::init())
	{
		return false;
	}

	objectClass = ObjectClass::ENEMY_OBJECT;
	
	scheduleUpdate();//启动定时器

	return true;
}

void BaseEnemy::update(float d)
{
	//敌人状态机
}


void BaseEnemy::idle(){}

void BaseEnemy::move(){}
void BaseEnemy::attack(){}
void BaseEnemy::checkAttack(){}
void BaseEnemy::endAttack(){}
void BaseEnemy::hurt(){}
void BaseEnemy::endHurt(){}

void BaseEnemy::death(){}
void BaseEnemy::endDeath(){};//结束死亡

void BaseEnemy::turning(){}