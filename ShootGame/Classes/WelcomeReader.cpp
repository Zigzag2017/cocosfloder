#include "WelcomeReader.h"
#include "Welcome.h"

USING_NS_CC;

static WelcomeReader* _instanceReader = nullptr;

WelcomeReader* WelcomeReader::getInstance()
{
	if (!_instanceReader)
	{
		_instanceReader = new WelcomeReader();
	}
	return _instanceReader;
}

void WelcomeReader::purge()
{
	CC_SAFE_DELETE(_instanceReader);
}

Node* WelcomeReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions)
{
	Welcome* node = Welcome::create();
	setPropsWithFlatBuffers(node, nodeOptions);
	return node;
}

