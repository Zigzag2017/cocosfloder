#ifndef __PARTICLE_CREATOR_H__
#define __PARTICLE_CREATOR_H__


#include "cocos2d.h"

using namespace cocos2d;
class BaseParticle;
class BaseStage;
class ParticleCreator
{
public:
	ParticleCreator(int initSize, int style);
	~ParticleCreator();
	BaseParticle* request();
	void putback(BaseParticle* particle);
	void garbage();

	void makeParticles(int targetZOrder, float x, float y, int bottom, int xSpeed);
	BaseStage* stage;
	void setStage(BaseStage* s);
	

private:
	void allocate(int size);
	static void freeParticle(void *particle);
	Vector<BaseParticle*>m_freeList;
	Vector<BaseParticle*>m_usedList;

	int m_initSize;
	int m_style;

};



#endif