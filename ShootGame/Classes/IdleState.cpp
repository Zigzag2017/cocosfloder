#include "IdleState.h"
#include "Hero.h"
#include "FiniteStateMachine.h"
/**
*空闲状态
*
*在启动函数播放动画，在chang时调用启动函数
*
*允许移动，但是移动后状态就会改变为MOVE   ->  移动 触发 MOVE
*允许攻击和技能，但是改变状态
*允许切枪和装弹
*/
void IdleState::execute(Hero *hero, MessageEnum messageEnum)
{
	log("000000000000000000");
	switch (messageEnum)
	{
	case MessageEnum::IDLE:
		break;
	case MessageEnum::MOVE:
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;
	case MessageEnum::FILL://开枪才填
		break;
	case MessageEnum::FIRE:
		if (hero->bullets <= 0)
		{
			hero->getFsm()->currentState = MessageEnum::FILL;
			hero->getFsm()->changeState();
			break;
		}
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		//hero->fire();
		break;
	/*case MessageEnum::TUMBLE:
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;
	case MessageEnum::START:
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;*/
	case MessageEnum::SKILL:
		if (hero->lines > 0)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	}
	//hero->idle();
}

void IdleState::run(Hero *hero)
{
	hero->idle();
	hero->currentName = "IdleState";
}