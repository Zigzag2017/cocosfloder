#ifndef __GAME_MANAGER_H__
#define __GAME_MANAGER_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"

class Hero;
class ArealCreator;

class GameManager : public cocos2d::Node
{
public:

	//GameManager();
	~GameManager();

	CREATE_FUNC(GameManager);

	virtual bool init();

	int heroDistance;//英雄总偏移

	//身后留1/3，面前留2/3
	int beginRect;//开始
	int endRect;//终点

	//根据面向分配，1/3与2/3，动态过程
	//暂时的镜头，需要移动的镜头，每多久移动多少
	int currentPoint;//当前坐标
	int targetPoint;//目标坐标，通过面向，分辨距离+位置

	int prePoint;//之前坐标

	int getTagetPoint();//判断面向，确定左右边距，在计算目标


	void darkness();


	int sceneWidth;

	Hero* hero;
	ArealCreator* arealCreator;

	void setup(Hero* h, ArealCreator* a);

	void gameUpdate(float dt);
	void update(float d);
	bool zoom;
	int lt;//差值

};


#endif // __GAME_MANAGER_H__