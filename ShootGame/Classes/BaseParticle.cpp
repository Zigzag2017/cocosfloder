#include "BaseParticle.h"
#include "ParticleCreator.h"

BaseParticle::BaseParticle(int style)
{
	m_style = style;
}



/*
*层级，起点，水平速度，自转系数，重力加速度
*
*/

void BaseParticle::used(ParticleCreator *pc, int targetZOrder, float x, float y, int bottom, int xSpeed, float rotation, Node* stage)
{
	creator = pc;

	lt_c = (rand() % 10)*0.1 + 0.2;

	setZOrder(targetZOrder);
	m_x = x;
	m_y = y;
	if (xSpeed>0)
		m_width = x + 150;
	else
		m_width = x - 150;
	m_bottom = bottom;
	m_xSpeed = xSpeed;
	m_rotation = rotation;
	this->stage = (BaseStage*)stage;

	m_ySpeed = 10;
	lt = 0;

	this->setPosition(Vec2(m_x, m_y));


	//加上舞台计时，记得不让回收
	stage->addChild(this);
	scheduleUpdate();
}
void BaseParticle::putback()
{
	if (this->getParent())
	this->removeFromParent();
	//*********************从池回收
	if (creator)
	creator->putback(this);
}



void BaseParticle::update(float d)
{
	lt -= lt_c;
	m_ySpeed += lt;
	
	if (this->getPositionY() <= m_bottom)
	{
		m_ySpeed = (m_ySpeed > 0 ? m_ySpeed : -1 * m_ySpeed) / 2;//速度向上，差值为负，速度递减
		//lt = lt > 0 ? -1 * lt : lt;
		lt = 0;
	}
	this->setRotation(this->getRotation()+10);

	this->setPositionX(this->getPositionX() + m_xSpeed);
	this->setPositionY(this->getPositionY() + m_ySpeed);

	if (m_xSpeed > 0)
	{
		this->setOpacity(int((m_width - this->getPositionX()) * 255 / 150.0));
		if(this->getPositionX() > m_width )
		{
			
			this->setOpacity(255);
			unscheduleUpdate();
			putback();
		}
	}
	else
	{
		this->setOpacity(int((this->getPositionX() - m_width) * 255 / 150.0));
		if (this->getPositionX() < m_width )
		{

			this->setOpacity(255);
			unscheduleUpdate();
			putback();
		}
	}
}