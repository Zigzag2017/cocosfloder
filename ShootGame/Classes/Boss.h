#ifndef __BOSS_ENEMY_H__
#define __BOSS_ENEMY_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "BaseEnemy.h"

USING_NS_CC;

class Hero;


class Boss :
	public BaseEnemy
{
public:

	Boss(float x, float y, Node* h);
	virtual bool init();
	//CREATE_FUNC(NormalEnemy);

	static Boss* create(float x, float y, Node* h)
	{
		Boss *pRet = new Boss(x, y, h);
		if (pRet && pRet->init())
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = nullptr;
			return nullptr;
		}
	};

	Hero *hero;
	//virtual void createFixture();
	//virtual void createShape();
	//virtual void createBody();
	Animation* moveAnimation;
	Animation* attackAnimation;
	Animation* hurtAnimation;

	GLProgramState* m_eGLProgramState;
	Vec4 disColor;

	void createShape();

	virtual void handleBeginContact(b2Contact* contact);
	virtual void handleEndContact(b2Contact* contact);
	/////////////////身体状态
	virtual void idle();//站立，待定

	virtual void move();//移动
	virtual void attack();//攻击
	virtual void checkAttack();
	virtual void endAttack();//攻击
	virtual void hurt();//受伤
	virtual void endHurt();//受伤

	virtual void death();//死亡
	virtual void endDeath();//结束死亡

	bool isShock;//是否休克
	void shock();//休克
	void endShock();//结束休克

	virtual void turning();//转身


	float fHeight;//浮动 高度
	float fCount;

	void setStage(BaseStage* s);

	bool isCatch;//是否捕获英雄，捕获到才可以攻击
	//cocostudio::timeline::SkeletonNode *bodyNode;
	//cocostudio::timeline::ActionTimeline *bodyAction; //身体骨骼动画
private:



	virtual void update(float d);
	/*int speed;//移动速度
	float moveAngle;//移动角度
	*/



};


#endif