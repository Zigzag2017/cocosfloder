#include "DarkLayer.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;


DarkLayer::DarkLayer()
:_center(Vec2(0.0f, 0.0f))
, _resolution(Vec2(0.0f, 0.0f))
, _time(0.0f)
{
	_rect_right_up = Director::getInstance()->getWinSize();
	_rect_left_down = Vec2(0.0, 0.0);
}

DarkLayer::~DarkLayer()
{
}

DarkLayer* DarkLayer::shaderNodeWithVertex(const std::string &vert, const std::string& frag)
{
	auto node = new (std::nothrow) DarkLayer();
	node->initWithVertex(vert, frag);
	node->autorelease();

	return node;
}

bool DarkLayer::initWithVertex(const std::string &vert, const std::string &frag)
{
	_vertFileName = vert;
	_fragFileName = frag;
#if CC_ENABLE_CACHE_TEXTURE_DATA
	auto listener = EventListenerCustom::create(EVENT_RENDERER_RECREATED, [this](EventCustom* event){
		this->setGLProgramState(nullptr);
		loadShaderVertex(_vertFileName, _fragFileName);
	});

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
#endif

	loadShaderVertex(vert, frag);

	_time = 0;

	auto s = Director::getInstance()->getWinSize();

	_resolution = Vec2(s.width, s.height);
	scheduleUpdate();

	setContentSize(Size(s.width, s.height));
	setAnchorPoint(Vec2(0.5f, 0.5f));


	return true;
}

void DarkLayer::loadShaderVertex(const std::string &vert, const std::string &frag)
{
	auto fileUtiles = FileUtils::getInstance();

	// frag
	auto fragmentFilePath = fileUtiles->fullPathForFilename(frag);
	auto fragSource = fileUtiles->getStringFromFile(fragmentFilePath);

	// vert
	std::string vertSource;
	if (vert.empty()) {
		vertSource = ccPositionTextureColor_vert;
	}
	else {
		std::string vertexFilePath = fileUtiles->fullPathForFilename(vert);
		vertSource = fileUtiles->getStringFromFile(vertexFilePath);
	}

	auto glprogram = GLProgram::createWithByteArrays(vertSource.c_str(), fragSource.c_str());
	auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
	setGLProgramState(glprogramstate);

	//创建地板所用的贴图。
	auto textrue1 = Director::getInstance()->getTextureCache()->addImage("light/pic.png");
	//将贴图设置给Shader中的变量值u_texture1
	glprogramstate->setUniformTexture("u_sea", textrue1);
}

void DarkLayer::update(float dt)
{
	if (this->isVisible())
	 _time += dt;
	// _time += 1;
	float value = (-1*cos(_time) + 1.0)*0.5;
	//if (value >= 0.95)this->setVisible(false);
	getGLProgramState()->setUniformFloat("tt", value);
	log("*************");
	log("*************");
	log("*************");
	log("*************");
}

void DarkLayer::setPosition(const Vec2 &newPosition)
{
	Node::setPosition(newPosition);
	auto position = getPosition();
	auto frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto retinaFactor = Director::getInstance()->getOpenGLView()->getRetinaFactor();
	_center = Vec2(position.x * frameSize.width / visibleSize.width * retinaFactor, position.y * frameSize.height / visibleSize.height * retinaFactor);
}

//uv.x+0.5;右边0.5平稳  uv.x+1.5;右边1.5平稳  uv.x-0.5;左边0.5平稳
//uv.y+0.5;向下
void DarkLayer::setHeroPosition(const cocos2d::Vec2 &newPosition, bool direction)
{
	if (direction)//朝向右边
	{
		_center.x = -1 * newPosition.x;
		_center.y = 1 - newPosition.y;
		_rect_right_up = Director::getInstance()->getWinSize();
		_rect_left_down = Vec2(Director::getInstance()->getWinSize().width * newPosition.x * 0.5, 0.0);
	}
	else
	{
		_center.x = 2 - newPosition.x;
		_center.y = 1 - newPosition.y;
		_rect_right_up = Vec2(Director::getInstance()->getWinSize().width * newPosition.x * 0.5, Director::getInstance()->getWinSize().height);
		_rect_left_down = Vec2(0.0, 0.0);

	}

	//_center = Vec2(0.0, 0.0);
}

void DarkLayer::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	_customCommand.init(_globalZOrder, transform, flags);
	_customCommand.func = CC_CALLBACK_0(DarkLayer::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);
}

void DarkLayer::onDraw(const Mat4 &transform, uint32_t flags)
{


	GLfloat vertices[12] = { _rect_left_down.x, 0, _rect_right_up.x, 0, _rect_right_up.x, _rect_right_up.y,
		_rect_left_down.x, 0, _rect_left_down.x, _rect_right_up.y, _rect_right_up.x, _rect_right_up.y };

	auto glProgramState = getGLProgramState();
	glProgramState->setUniformVec2("resolution", _resolution);
	glProgramState->setUniformVec2("center", _center);
	glProgramState->setVertexAttribPointer("a_position", 2, GL_FLOAT, GL_FALSE, 0, vertices);

	glProgramState->apply(transform);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	CC_INCREMENT_GL_DRAWN_BATCHES_AND_VERTICES(1, 6);
}


