#include "BackgroundManager.h"
#include "Hero.h"
#include "BaseStage.h"
#include "BubbleCreator.h"
#include "BaseBubble.h"
#include "GameManager.h"


BackgroundManager::~BackgroundManager()
{

}

void BackgroundManager::updatePosition(float l)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	this->setPositionX(l);
	float a, b;

	if (stage->gameManager->currentPoint < 6000)
	{
		a = 0;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 14000)
	{
		a = visibleSize.width * 304.7 / 20;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 22000)
	{
		a = visibleSize.width * 304.7 / 20;
		b = visibleSize.width * 304.7 / 20;
	}
	else if (stage->gameManager->currentPoint < 30000)
	{
		a = visibleSize.width * 304.7 / 10;
		b = visibleSize.width * 304.7 / 20;
	}
	else if (stage->gameManager->currentPoint < 38000)
	{
		a = visibleSize.width * 304.7 / 10;
		b = visibleSize.width * 304.7 / 10;
	}
	floorA->setPositionX(visibleSize.width * 10.0 / 40 + a + l);
	floorB->setPositionX(visibleSize.width * 314.7 / 40 + b + l);
	
	
	if (stage->gameManager->currentPoint < 7800)
	{
		a = 0;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 16000)
	{
		a = 15332;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 23000)
	{
		a = 15332;
		b = 15332;
	}
	else if (stage->gameManager->currentPoint < 31000)
	{
		a = 30664;
		b = 15332;
	}
	else if (stage->gameManager->currentPoint < 39000)
	{
		a = 30664;
		b = 30664;
	}
	
	flowerA->setPositionX(600 + a + l);
	flowerB->setPositionX(8266+ b + l);

	
	if (stage->gameManager->currentPoint < 4400)
	{
		a = 0;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 9000)
	{
		a = 15180;
		b = 0;
	}
	else if (stage->gameManager->currentPoint < 13600)
	{
		a = 15180;
		b = 15180;
	}
	else if (stage->gameManager->currentPoint < 18000)
	{
		a = 30360;
		b = 15180;
	}
	else if (stage->gameManager->currentPoint < 22400)
	{
		a = 30360;
		b = 30360;
	}
	else if (stage->gameManager->currentPoint < 27000)
	{
		a = 45540;
		b = 30360;
	}
	else if (stage->gameManager->currentPoint < 31500)
	{
		a = 45540;
		b = 45540;
	}

	else if (stage->gameManager->currentPoint < 35000)
	{
		a = 60720;
		b = 45540;
	}
	else
	{
		a = 60720;
		b = 60720;
	}

	
	stoneA->setPositionX(600 + a + l*1.70);
	stoneB->setPositionX(8190 + b + l*1.70);
	//skyA->setPositionX(visibleSize.width * 5.0 / 40 + l);
	bubbleLayer->setPositionX(l);
	//m_particleSystem->setPositionX(visibleSize.width / 2 + l);
}

bool BackgroundManager::init()
{


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//background = Sprite::create("bg.png");
	//background->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 ));
	//background->setGlobalZOrder(-1);
	//this->addChild(background);

	//floorLayer = Sprite::create();
	//this->addChild(floorLayer);

	//flowerLayer = Sprite::create();
	//this->addChild(flowerLayer);
	//
	//auto	fGLProgram = GLProgram::createWithFilenames("flower.vsh", "flower.fsh");
	//auto m_fGLProgramState = GLProgramState::create(fGLProgram);
	/*for (int i = 0; i < 50; i++)
	{
	Sprite *s = Sprite::create("flower.png");
	s->setScale((rand()%100/100.0)*1+2);
	s->setPosition(Vec2(visibleSize.width * 2.0 * i / 50, visibleSize.height / 2 + 80));

	//m_fGLProgramState->setUniformVec4("v_LightColor", tLightColor);
	//s->setGLProgramState(m_fGLProgramState);
	//Vec4  tLightColor((rand() % 100 / 100.0), 1.0, 1.0, 1.0);
	//float h = i / 5 * 50.0;
	//m_fGLProgramState->setUniformFloat("v_dH",h);
	//m_fGLProgramState->applyUniforms();
	flowerLayer->addChild(s);
	}*/


	//flowerLayer->setVisible(false);

	//skyA = Sprite3D::create("map.obj");
	//skyA->setScale(40.0);
	//skyA->setScaleX(60.0);
	//skyA->setRotation3D(Vec3(90, 0, 0));
	//skyA->setPosition3D(Vec3(visibleSize.width * 5.0 / 40.0, visibleSize.height * 0.4f, visibleSize.height / 2 -4000 ));
	//this->addChild(skyA);

	//auto zz = Sprite::create("flower/bg.png");
	//zz->setGlobalZOrder(-1000);
	//this->addChild(zz);


	floorA = Sprite3D::create("flower/map.obj");
	floorA->setScale(40.0);
	floorA->setScaleX(60.0);
	floorA->setRotation3D(Vec3(10, 0, 0));
	floorA->setPosition3D(Vec3(visibleSize.width * 10.0 / 40, 100, visibleSize.height / 2 - 1200));

	floorB = Sprite3D::create("flower/map.obj");
	floorB->setScale(40.0);
	floorB->setScaleX(60.0);
	floorB->setRotation3D(Vec3(10, 0, 0));
	floorB->setPosition3D(Vec3(visibleSize.width * 78.0 / 10, 100, visibleSize.height / 2 - 1200));

	//floorA->setVisible(false);
	//floorB->setVisible(false);

	this->addChild(floorA);
	//floorLayer
	this->addChild(floorB);



	float zeye = Director::getInstance()->getZEye();

	auto _camera = Camera::createPerspective(60, (GLfloat)visibleSize.width / visibleSize.height, 10, zeye + visibleSize.height * 6.0f);
	_camera->setCameraFlag(CameraFlag::USER1);

	Vec3 eye(visibleSize.width / 2, visibleSize.height / 2.0f, zeye), center(visibleSize.width / 2, visibleSize.height / 2, 0.0f), up(0.0f, 1.0f, 0.0f);
	setPosition3D(eye);
	_camera->setPosition3D(Vec3(visibleSize.width / 2, visibleSize.height / 2, 400));
	_camera->lookAt(center, up);
	this->addChild(_camera);

	floorA->setCameraMask((unsigned short)CameraFlag::USER1);
	floorB->setCameraMask((unsigned short)CameraFlag::USER1);
	//	skyA->setCameraMask((unsigned short)CameraFlag::USER1);
	//skyB->setCameraMask((unsigned short)CameraFlag::USER1);

	//background->setCameraMask((unsigned short)CameraFlag::USER1);

	auto	pGLProgram = GLProgram::createWithFilenames("UVAnimation.vsh", "UVAnimation.fsh");
	m_pGLProgramState = GLProgramState::create(pGLProgram);

	floorA->setGLProgramState(m_pGLProgramState);
	floorB->setGLProgramState(m_pGLProgramState);

	//	auto	flowerGLProgram = GLProgram::createWithFilenames("flower/bg.vsh", "flower/bg.fsh");
	//	m_flowerGLProgramState = GLProgramState::create(flowerGLProgram);
	////	skyA->setGLProgramState(m_flowerGLProgramState);
	//	//skyB->setGLProgramState(m_flowerGLProgramState);
	//
	//	auto pTexFlower = Director::getInstance()->getTextureCache()->addImage("flower/bg.png");
	//	//将贴图设置给Shader中的变量值u_texture1
	//	m_flowerGLProgramState->setUniformTexture("u_texture", pTexFlower);


	//创建地板所用的贴图。
	auto textrue1 = Director::getInstance()->getTextureCache()->addImage("flower/floorTiled.png");
	//将贴图设置给Shader中的变量值u_texture1
	m_pGLProgramState->setUniformTexture("u_texture", textrue1);


	//创建波光贴图。
	auto pTexLight = Director::getInstance()->getTextureCache()->addImage("flower/caustics.png");
	//将贴图设置给Shader中的变量值u_lightTexture
	m_pGLProgramState->setUniformTexture("u_lightTexture", pTexLight);

	//纹理UV寻址方式为GL_REPEAT。
	Texture2D::TexParams		tRepeatParams;
	tRepeatParams.magFilter = GL_LINEAR_MIPMAP_LINEAR;
	tRepeatParams.minFilter = GL_LINEAR;
	tRepeatParams.wrapS = GL_REPEAT;
	tRepeatParams.wrapT = GL_REPEAT;
	pTexLight->setTexParameters(tRepeatParams);

	//在这里，我们设置一个波光的颜色，这里设置为白色。
	Vec4  tLightColor(1.0, 1.0, 1.0, 0.6);
	m_pGLProgramState->setUniformVec4("v_LightColor", tLightColor);




	//下面这一段，是为了将我们自定义的Shader与我们的模型顶点组织方式进行匹配。模型的顶点数据一般包括位置，法线，色彩，
	//纹理，以及骨骼绑定信息。而Shader需要将内部相应的顶点属性通道与模型相应的顶点属性数据进行绑定才能正确显示出顶点。
	//long offset = 0;
	//auto attributeCount = floorA->getMesh()->getMeshVertexAttribCount();
	//for (auto k = 0; k < attributeCount; k++)
	//{
	//	auto meshattribute = floorA->getMesh()->getMeshVertexAttribute(k);
	//	m_pGLProgramState->setVertexAttribPointer(
	//		s_attributeNames[meshattribute.vertexAttrib],
	//		meshattribute.size,
	//		meshattribute.type,
	//		GL_FALSE,
	//		floorA->getMesh()->getVertexSizeInBytes(),
	//		(GLvoid*)offset
	//		);

	//	offset += meshattribute.attribSizeBytes;
	//}

	//uv滚动初始值设为0
	m_LightAni.x = m_LightAni.y = 0;



	auto sky = Sprite::create("flower/sky.png");
	sky->setScale(5.56);
	this->addChild(sky);//泡泡进去

	sky->setPosition3D(Vec3(visibleSize.width * 20.0 / 40.0, visibleSize.height * 2.0f, visibleSize.height / 2 - 3000));
	sky->setCameraMask((unsigned short)CameraFlag::USER1);

	flowerLayer = Sprite::create();
	this->addChild(flowerLayer);

	flowerA = Sprite::create("flower/stone.png");

	flowerA->setScale(8);
	flowerLayer->addChild(flowerA);//花进去
	flowerA->setPosition3D(Vec3(600, -1750, -1100)); // (600, -1750, -1100));
	//flowerLayer->setVisible(false);

	flowerB = Sprite::create("flower/stone.png");

	flowerB->setScale(8);
	flowerLayer->addChild(flowerB);//花进去
	flowerB->setPosition3D(Vec3(8266, -1750, -1100));



	stoneA = Sprite::create("flower/stone2.png");

	stoneA->setScale(8);
	flowerLayer->addChild(stoneA);//花进去
	stoneA->setPosition3D(Vec3(600, -2000, -900));

	//stoneA->setVisible(false);
	//flowerLayer->setVisible(false);

	stoneB = Sprite::create("flower/stone2.png");

	stoneB->setScale(8);
	flowerLayer->addChild(stoneB);//花进去
	stoneB->setPosition3D(Vec3(8190, -2000, -900));


	bubbleLayer = Sprite::create();
	flowerLayer->addChild(bubbleLayer);//泡泡进去

	//m_particleSystem = CCParticleSystemQuad::create("particle/bubble.plist");//移动层，层上目的地不该移动
	//m_particleSystem->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 2600));
	//m_particleSystem->setAutoRemoveOnFinish(true);
	//m_particleSystem->setScale(5);
	//bubbleLayer->addChild(m_particleSystem);
	//bubbleLayer->addChild(m_particleSystem);

	for (int i = 0; i < 20; i++)
	{
		BaseBubble* p = BaseBubble::create("flower/baseBubble.png", 1);
		//p->setScale(10);
		p->setTag(100 + i);
		p->setPosition(Vec2(-4 * 480, -7 * 320));
		bubbleLayer->addChild(p);
		p->setVisible(false);
	}

	bubbleCreator = new BubbleCreator(8, bubbleLayer);
	
	//bubbleCreator->setStage();




	//Sprite* p2 = Sprite::create("flower/bubble.png");
	//p2->setScale(1);
	//p2->setPosition(Vec2(480*6, -320 * 3));
	//bubbleLayer->addChild(p2);

	//Sprite* p3 = Sprite::create("flower/bubble.png");
	//p3->setScale(1);
	//p3->setPosition(Vec2(480 * 16, -320 * 3));
	//bubbleLayer->addChild(p3);

	flowerLayer->setPosition3D(Vec3(visibleSize.width * 5.0 / 40.0, visibleSize.height * 4.3f, visibleSize.height / 2 - 3000));
	flowerLayer->setCameraMask((unsigned short)CameraFlag::USER1);



	this->scheduleUpdate();

	return true;
}

void BackgroundManager::setup(Hero* h, BaseStage* s)
{


	hero = h;
	stage = s;
	bubbleCreator->setBaseStage(stage);

	auto seq = Sequence::create(DelayTime::create(1), CallFuncN::create(CC_CALLBACK_1(BackgroundManager::makeBubble, this)), NULL);
	this->runAction(seq);

	auto seq2 = Sequence::create(DelayTime::create(1.5), CallFuncN::create(CC_CALLBACK_1(BackgroundManager::makeBubble, this)), NULL);
	this->runAction(seq2);

	auto seq3 = Sequence::create(DelayTime::create(2), CallFuncN::create(CC_CALLBACK_1(BackgroundManager::makeBubble, this)), NULL);
	this->runAction(seq3);

	//schedule(schedule_selector(GameManager::gameUpdate), 0.02f);
}

void BackgroundManager::update(float delta)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	m_LightAni.y -= 0.0002f;
	if (m_LightAni.y < 0.0f)
	{
		m_LightAni.y += 1.0f;
	}

	m_LightAni.x -= 0.0002f;
	if (m_LightAni.x < 0.0f)
	{
		m_LightAni.x += 1.0f;
	}

	m_pGLProgramState->setUniformVec2("v_animLight", m_LightAni);



	//m_particleSystem->setPositionX(10 * visibleSize.width*(random() % 100) / 100.0 - 2.0*visibleSize.width);

	//bubbleLayer->setPositionX(bubbleLayer->getPositionX()+10);




}

void BackgroundManager::makeBubble(Node* node)
{

	//log("zzz*********************************");

	bubbleCreator->makeParticles(this->getZOrder(), CCRANDOM_0_1() * 960, 0, 400 + 10, -1);

	auto seq = Sequence::create(DelayTime::create(0.5 + (CCRANDOM_0_1() * 15)*0.1), CallFuncN::create(CC_CALLBACK_1(BackgroundManager::makeBubble, this)), NULL);
	this->runAction(seq);
}

void BackgroundManager::closeLight()
{
	Vec4  tLightColor(1.0, 1.0, 1.0, 1.0);
	m_pGLProgramState->setUniformVec4("v_LightColor", tLightColor);

	bubbleLayer->setVisible(false);//泡消失
}