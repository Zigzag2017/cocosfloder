#include "Welcome.h"
#include "ui\UIText.h"
#include "WelcomeScene.h"
#include "BaseStage.h"

USING_NS_CC;
using namespace std;
using namespace cocos2d::ui;

Welcome::Welcome()
{}

// typedef std::function<void(Ref*,Widget::TouchEventType)> ccWidgetTouchCallback;

Widget::ccWidgetTouchCallback Welcome::onLocateTouchCallback(const std::string &callBackName)
{
	if (callBackName == "onTouch")
	{
		return CC_CALLBACK_2(Welcome::onTouch, this);
	}
	return nullptr;
}

Widget::ccWidgetClickCallback Welcome::onLocateClickCallback(const std::string &callBackName)
{
	if (callBackName == "startGame")
	{
		return CC_CALLBACK_1(Welcome::onClick, this);
	}
	else if(callBackName == "continueGame")
	{
		return CC_CALLBACK_1(Welcome::onClick2, this);
	}
	return nullptr;
}

Widget::ccWidgetEventCallback Welcome::onLocateEventCallback(const std::string &callBackName)
{
	if (callBackName == "onEvent")
	{
		return CC_CALLBACK_2(Welcome::onEvent, this);
	}
	return nullptr;
}

void Welcome::onTouch(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	
	log("onTouch");
}

void Welcome::onClick(cocos2d::Ref* sender)
{
	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5, BaseStage::createScene()));

	

	log("onClick");
}

void Welcome::onClick2(cocos2d::Ref* sender)
{
	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5, BaseStage::createScene()));

	//auto scene = BaseStage::createScene();
	log("onClick");
}

void Welcome::onEvent(cocos2d::Ref* sender, int eventType)
{
	log("onEvent");
}
