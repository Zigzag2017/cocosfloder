#ifndef __MOVE_STATE_H_  
#define __MOVE_STATE_H_  
#include "State.h"  
class MoveState :public State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum);
	virtual void run(Hero *hero);
};

#endif  