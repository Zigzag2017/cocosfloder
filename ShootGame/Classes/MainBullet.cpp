#include "MainBullet.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"//关卡类基类，子弹添加场景，获取子弹制造者
#include "GameManager.h"
#include "Hero.h"

USING_NS_CC;



MainBullet::MainBullet(int style)
{
	
	//_x = x;
	//_y = y;
	_x = 0;
	_y = 0;
	m_style = style;

	heroHeight = 0.0;

	this->init();

	log("run MainBullet -> yes");

}

void MainBullet::used(b2Vec2 vec, float speed, Node* stage)
{
	//_body->GetPosition().Set(x,y);

	//_body->SetTransform(b2Vec2(100,100),0);

	//_body->GetPosition() = b2Vec2(100,100);

	//body->set
	//_x = 0;
	//_y = 0;
	
	this->speed = speed;

	log("%f", _body->GetAngle());

	isActive = true;

	_body->SetTransform(vec, 0);

	//objectClass = ObjectClass::BULLET_OBJECT;

	_body->SetLinearVelocity(b2Vec2(speed,0));
//	_body->ApplyLinearImpulse(b2Vec2(100,100),_body->GetWorldCenter(),true);

	

	//BaseState* baseState = ;
	((BaseStage*)stage)->objectLayer->addChild(this);
	this->stage = (BaseStage*)stage;


	heroHeight = this->stage->hero->fHeight;

	scheduleUpdate();//启动定时器

	log("%f", body->getPosition().x);

	//this->setPositionX(_body->GetPosition().x * 32.0);
	//this->setPositionY(_body->GetPosition().y * 16.0 + 90);
	//发射

	//const b2Vec2 &z = b2Vec2(0, 0);
	//z.Set(1,1);
	
	
}
void MainBullet::putback()
{
	
	_body->SetAwake(false);
	//_body->SetTransform(b2Vec2(0, 0), 0);
	//_body->SetLinearVelocity(b2Vec2(0, 0));
	//_body->SetActive(false);
	
	unscheduleUpdate();

	if (this->getParent())
	{
		//stage = (BaseStage*)this->getParent()->getParent();
		this->removeFromParent();
		stage->bulletCreator->putback(this);
		//isActive = false;
	}
}

void MainBullet::update(float d)
{

	//heroHeight = 0;

	this->setPositionX(_body->GetPosition().x * 32.0 - this->stage->gameManager->currentPoint);
	this->setPositionY(_body->GetPosition().y * 5.536 + 110 + 150 + heroHeight);

	this->setZOrder(1000 - (int)_body->GetPosition().y);
}

void MainBullet::handleBeginContact(b2Contact* contact)//不让信号子弹被拦截，不然后无法回收撞墙，过滤
{
	Box2DPhysicsObject* other = (this == contact->GetFixtureA()->GetBody()->GetUserData()) ? (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData() : (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
	
	if (other->objectClass == ObjectClass::WALL_OBJECT){
		this->isActive = false;
		putback();//拦截子弹
		log("meet well stop!!");
	}
	
	if (isActive)
	{
		
		//if (other->objectclass == objectclass::enemy_object){
		//	putback();//筛选是不是，显示爆炸特效，或者穿过
		//	log("meet enemy boom!!");
		//}
		//
		//if (other->objectclass == objectclass::insulate_object){
		//	putback();//筛选是不是，显示爆炸特效，或者穿过
		//	log("meet insulate well stop!!");
		//}
		

	}
	else
	{
		
	}

}

void MainBullet::createFixture()
{
	log("MainBullet createFixture");

	//bodyDef.linearDamping = 1;



	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;
	fixtureDef.isSensor = true;

	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);
}

void MainBullet::createShape()
{
	log("MainBullet createShape");


	b2PolygonShape *shape = new b2PolygonShape();
	shape->SetAsBox(10 / 2 / 32.0, 60 / 2 / 32.0);
	_shape = shape;


}

void MainBullet::createBody()
{
	log("BaseBullet createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_kinematicBody;
	bodyDef.position.Set(_x / 32.0, _y / 32.0);
	bodyDef.fixedRotation = true;
	bodyDef.bullet = true;
	//bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
}

bool MainBullet::init()
{

	if (!BaseBullet::init())
	{
		return false;
	}

	body = Sprite::createWithSpriteFrameName("P/bullet.png");
	body->setScale(2);
	this->addChild(body);


	//scheduleUpdate();//启动定时器

	return true;
}