#ifndef __HERO__H__
#define __HERO__H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Box2DPhysicsObject.h"


USING_NS_CC;

class FiniteStateMachine;
class BaseStage;
class LineBullet;

class Hero :
	public Box2DPhysicsObject
{
public:
	

	Hero();
	virtual bool init();
	CREATE_FUNC(Hero);

	FiniteStateMachine*getFsm();


	virtual void createFixture();
	virtual void createShape();
	virtual void createBody();

	virtual void handleBeginContact(b2Contact* contact);

	/////////////////身体状态
	void idle();
	void move();
	//void start();
	void fill();
	void fire();
	void tumble(Ref* r);
	void skill();

	////////////////脚部状态
	void forward();//前进
	void backward();//后退
	void stand();//站立
	//void footTumble();//摔倒

	void hurt();
	void endHurt(Node *node);

	void setBodyDirection();
	void setMoveDirection();

	void startMove(float a);//开始移动，设置移动角度
	void endMove();


	float rightCurrentPoint;
	float leftCurrentPoint;


	//外部访问属性，注意安全

	bool isFill;
	bool addBullet;

	bool doFire;//进行发射，子弹出现时为false，为false是则可以改变动作，比如连续射击或者切枪，技能

	bool isHurt;//是否处于受伤，进入保护状态
	
	bool direction;//方向 right-true
	bool dirLock;//方向锁  攻击按紧锁方向 且处于攻击状态

	bool canControl;//能否控制，先判断能否控制
	bool isMove;//是否移动，摇杆是否处于移动状态

	BaseStage *stage;
	LineBullet *lineBullet;

	void setStage(BaseStage *s);

	Sprite* shadow;

	Node* face;

	int lifes;//生命值
	int bullets;//子弹数
	int lines;//能量数
	bool isLine;//能量炮是否开启

	bool isBack;
	bool isForward;

	float fHeight;//浮动 高度
	float fCount;

	bool lockPosition;

	std::string currentName;

	cocos2d::ui::ImageView* image_hp;
	cocos2d::ui::ImageView* image_mp;
	cocos2d::ui::ImageView* image_big_mp;

	void setImage(cocos2d::ui::ImageView* hp, cocos2d::ui::ImageView* mp, cocos2d::ui::ImageView* big_mp);

	/*临时*/
	float lastCos;
	float currentCos;

	float lastP;
	float currentP;

	bool isM;

private:

	inline void update(float d);
	inline void updatePosition();

	cocostudio::timeline::ActionTimeline *bodyAction; //身体骨骼动画
	cocostudio::timeline::ActionTimeline *handAction; //手骨骼动画
	FiniteStateMachine *mFsm;//状态机

	cocostudio::timeline::SkeletonNode *bodyNode;//身体节点
	cocostudio::timeline::SkeletonNode *handNode;//手节点


	int speed;//移动速度
	float moveAngle;//移动角度


	

};


#endif