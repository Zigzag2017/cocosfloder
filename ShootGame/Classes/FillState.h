#ifndef __FILL_STATE_H_  
#define __FILL_STATE_H_  
#include "State.h"  
class FillState :public State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum);
	virtual void run(Hero *hero);
};

#endif  