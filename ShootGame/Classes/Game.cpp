#include "Game.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "VirtualHandle.h"
#include <math.h>
#include "Hero.h"
#include "Box2DManager.h"
#include "NormalEnemy.h"
#include "MainBullet.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//**  you can create scene with following comment code instead of using csb file.
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/*
	// add "HelloWorld" splash screen"
	auto sprite = Sprite::create("HelloWorld.png");

	// position the sprite on the center of the screen
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	// add the sprite as a child to this layer
	this->addChild(sprite, 0);
	*/
	Box2DManager::getInstance()->init();/////////////////////////场景消失时销毁
	_world = Box2DManager::getInstance()->getWorld();//new b2World(b2Vec2(0,0));
	
	//contactListener = new Box2DContactListener();//记得删除
	//_world->SetContactListener(contactListener);
	

	//_hero = this->createObjet(visibleSize.width / 2, visibleSize.height / 2, 48.0f);

	/*
	auto ball1 = this->createObjet(visibleSize.width / 9 * 7, visibleSize.height / 2, 48.0f);
	ball1->SetLinearDamping(100.0);
	

	auto ball2 = this->createObjet(visibleSize.width / 9 * 6, visibleSize.height / 2, 48.0f);
	ball2->SetLinearDamping(100.0);

	auto ball3 = this->createObjet(visibleSize.width / 9 * 8, visibleSize.height / 2, 48.0f);
	ball3->SetLinearDamping(100.0);

	auto e =  NormalEnemy::create();
	this->addChild(e);*/
	//ball->ApplyLinearImpulse(b2Vec2(0,30),ball->GetLocalCenter(),false);

	enemyCreator = new EnemyCreator();//记得从堆删除
	//根据关卡AI匹配
	enemyCreator->addNormalEnemy(this, visibleSize.width / 9 * 6, visibleSize.height / 2);
	enemyCreator->addNormalEnemy(this, visibleSize.width / 9 * 7, visibleSize.height / 2);
	enemyCreator->addNormalEnemy(this, visibleSize.width / 9 * 8, visibleSize.height / 2);

	arealCreator = new ArealCreator();//记得从堆删除

	bulletCreator = new BulletCreator(8,1);
	/*
	auto b1 = bulletCreator->request();
	this->addChild(b1);

	auto b2 = bulletCreator->request();
	this->addChild(b2);

	auto b3 = bulletCreator->request();
	this->addChild(b3);

	this->removeChild(b2);

	this->removeChild(b3);
	bulletCreator->putback(b2);
	bulletCreator->putback(b3);
	auto b4 = bulletCreator->request();
	this->addChild(b4);

	*/

	//auto b = MainBullet::create(300,300);
	//this->addChild(b);
	/*
	//墙壁
	this->createBox(visibleSize.width / 2, 0, visibleSize.width ,10);

	this->createBox(visibleSize.width / 2, visibleSize.height, visibleSize.width, 10);


	this->createBox(visibleSize.width, visibleSize.height / 2, 10, visibleSize.height);

	this->createBox(0, visibleSize.height / 2, 10, visibleSize.height);
	*/
	// we get a reference to the actual velocity vector
	//b2Vec2 velocity = ball->GetLinearVelocity();
	//velocity.y = 100;

	//_hero->SetLinearVelocity(b2Vec2(0, 100));
	
	
	auto hero = Hero::create();
	log("--------------------------ok");


	hero->setPosition(Vec2(visibleSize.width / 3, visibleSize.height / 2));

	this->addChild(hero);
	
	//hero->setWorld(_world);

	_hero = hero->_body;

	//调试视图
	_debugDraw = new GLESDebugDraw(32);

	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	//flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_aabbBit;
	//flags += b2Draw::e_pairBit;
	//flags += b2Draw::e_centerOfMassBit;

	_debugDraw->SetFlags(flags);
	_world->SetDebugDraw(_debugDraw);


	auto v = VirtualHandle::create();
	//
	v->setPositionY(visibleSize.height / 3);

	addChild(v);

	/*
	hero->runAction(Sequence::create(DelayTime::create(5), CallFunc::create([this]()
	{
	auto l = this->hero;

	//this->hero->setPosition(Vec2(200, 200));
	}
	), NULL));*/

	v->setCallback([this](VirtualHandleEvent event, float data)
	{
		std::string value;

		switch (event)
		{
		case VirtualHandleEvent::LEFT_ATTACK_BTN_DWON:
			bulletCreator->request()->used(this->_hero->GetPosition(),0,this);//发射子弹
			log("LEFT_DOWN");
//			hero->isLeft = true;
//			hero->isRight = false;
//			hero->getFsm()->execute(MessageEnum::FIRE);
			break;

		case VirtualHandleEvent::LEFT_ATTACK_BTN_UP:
			log("LEFT_UP");
			break;

		case VirtualHandleEvent::RIGHT_ATTACK_BTN_DWON:
			log("RIGHT_DOWN");
//			hero->isRight = true;
//			hero->isLeft = false;
//			hero->getFsm()->execute(MessageEnum::FIRE);
			break;

		case VirtualHandleEvent::RIGHT_ATTACK_BTN_UP:
			log("RIGHT_UP");
			break;

		case VirtualHandleEvent::TOUCH_MOVE:
//			hero->startMove(data);//移动角度
//			hero->getFsm()->execute(MessageEnum::MOVE);//让fsm处理
			this->_hero->SetLinearVelocity(b2Vec2(cos(data)*10, sin(data)*10));
			break;
		case VirtualHandleEvent::END_MOVE:
//			hero->getFsm()->execute(MessageEnum::IDLE);
//			hero->endMove();
			this->_hero->SetLinearVelocity(b2Vec2(0,0));
			break;
		default:
			break;
		}

	});





	this->scheduleUpdate();


	

	return true;
}



b2Body* GameScene::createBox(float posX, float posY, float width,float height)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_staticBody;
	bodyDef.position.Set(posX  / 32.0, posY  / 32.0);
	bodyDef.fixedRotation = true;

	b2PolygonShape shape;
	shape.SetAsBox(width / 2 / 32.0, height / 2 / 32.0);

	b2FixtureDef fixtureDef;
	fixtureDef.density = 100;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = &shape;
	

	auto box = _world->CreateBody(&bodyDef);
	box->CreateFixture(&fixtureDef);

	return box;

}

b2Body* GameScene::createObjet(float posX, float posY, float radius)
{

	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(posX / 32.0, posY / 32.0);
	bodyDef.fixedRotation = true;
	//bodyDef.linearDamping = 1;

	b2CircleShape shape;
	shape.m_radius = radius / 32.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.density = 100;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = &shape;

	auto object = _world->CreateBody(&bodyDef);
	object->CreateFixture(&fixtureDef);

	return object;
}

void GameScene::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
	GL::enableVertexAttribs(cocos2d::GL::VERTEX_ATTRIB_FLAG_POSITION);

	_world->DrawDebugData();

	CHECK_GL_ERROR_DEBUG();
}

void GameScene::update(float dt)
{
	_world->Step(1.0f/60.0f,10,10);
}
