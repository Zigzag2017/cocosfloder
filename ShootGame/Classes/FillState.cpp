#include "FillState.h"
#include "Hero.h"
#include "FiniteStateMachine.h"

//中断填充，倒地，迫击，技能

//空闲和走路，无法中断改状态

void FillState::execute(Hero *hero, MessageEnum messageEnum)
{
	switch (messageEnum)
	{
	case MessageEnum::IDLE:
		if (hero->isFill == false)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	case MessageEnum::MOVE:
		if (hero->isFill == false)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	case MessageEnum::FILL://开枪才填
		break;
	case MessageEnum::FIRE:
		//if (hero->bullets > 0)
		//{
		//	hero->getFsm()->currentState = messageEnum;
		//	hero->getFsm()->changeState();
		//}
		//hero->fire();
		break;
	case MessageEnum::TUMBLE:
		//hero->getFsm()->currentState = messageEnum;
		//hero->getFsm()->changeState();
		break;
	case MessageEnum::START:
		//hero->getFsm()->currentState = messageEnum;
		//hero->getFsm()->changeState();
		break;
	case MessageEnum::SKILL:
		//if (hero->lines > 0)
		//{
		//	hero->getFsm()->currentState = messageEnum;
		//	hero->getFsm()->changeState();
		//}
		break;
	}
}

void FillState::run(Hero *hero)
{
	//hero->move();
	hero->currentName = "FillState";
	hero->fill();
	//auto seq = Sequence::create(DelayTime::create(3), CallFunc::create([hero](){
	//	hero->bullets = 100;/////////////////////////被打断后不可以填充，但是走路空闲不会打断
	//}), NULL);
	//seq->setTag(100);
	//hero->runAction(seq);
}