#include "EnemyCreator.h"
#include "NormalEnemy.h"
#include "BaseStage.h"
#include "Boss.h"

EnemyCreator::EnemyCreator()
{

}


EnemyCreator::~EnemyCreator()
{

}

void EnemyCreator::addNormalEnemy(Node* mainView,float x,float y)
{
	BaseStage *stage = (BaseStage*)mainView;

	Node* hero = (Node*)stage->_hero->GetUserData();

	auto normalEnemy = NormalEnemy::create(x,y,hero);
	normalEnemy->setStage((BaseStage*)mainView);
	stage->objectLayer->addChild(normalEnemy);

	


	//auto	fGLProgram = GLProgram::createWithFilenames("flower.vsh", "flower.fsh");
	//auto m_fGLProgramState = GLProgramState::create(fGLProgram);

	////m_fGLProgramState->setUniformVec4("v_LightColor", tLightColor);
	//normalEnemy->setGLProgramState(m_fGLProgramState);
	//Vec4  tLightColor((rand() % 100 / 100.0), 1.0, 1.0, 1.0);
	//float h = 10 / 5 * 50.0;
	//m_fGLProgramState->setUniformFloat("v_dH",h);
	//m_fGLProgramState->applyUniforms();

}

void EnemyCreator::addBoss(Node* mainView, float x, float y)
{
	BaseStage *stage = (BaseStage*)mainView;

	Node* hero = (Node*)stage->_hero->GetUserData();

	auto normalEnemy = Boss::create(x, y, hero);
	normalEnemy->setStage((BaseStage*)mainView);
	stage->objectLayer->addChild(normalEnemy);

	//auto	fGLProgram = GLProgram::createWithFilenames("flower.vsh", "flower.fsh");
	//auto m_fGLProgramState = GLProgramState::create(fGLProgram);

	////m_fGLProgramState->setUniformVec4("v_LightColor", tLightColor);
	//normalEnemy->setGLProgramState(m_fGLProgramState);
	//Vec4  tLightColor((rand() % 100 / 100.0), 1.0, 1.0, 1.0);
	//float h = 10 / 5 * 50.0;
	//m_fGLProgramState->setUniformFloat("v_dH",h);
	//m_fGLProgramState->applyUniforms();
}