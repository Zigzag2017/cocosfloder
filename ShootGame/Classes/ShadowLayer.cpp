#include "ShadowLayer.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "VirtualHandle.h"
#include <math.h>
#include "Hero.h"
#include "Box2DManager.h"
#include "NormalEnemy.h"
#include "MainBullet.h"
#include "FiniteStateMachine.h"

USING_NS_CC;

using namespace cocostudio::timeline;

bool ShadowLayer::init()
{
	//**  you can create scene with following comment code instead of using csb file.
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	return true;
}