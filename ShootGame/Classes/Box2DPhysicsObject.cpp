#include "Box2DPhysicsObject.h"


#include "ui/CocosGUI.h"
#include <math.h>
#include "Box2DManager.h"

USING_NS_CC;



bool Box2DPhysicsObject::init()
{
	

	if (!Sprite::init())
	{
		return false;
	}

	log("Box2DPhysicsObject init");

	_beginContactCallEnabled = true;
	_endContactCallEnabled = false;
	_preContactCallEnabled = false;
	_postContactCallEnabled = false;

	_world = Box2DManager::getInstance()->getWorld();

	this->createBody();
	this->createShape();
	this->createFixture();
	
	_body->SetUserData(this);

	return true;
}

bool Box2DPhysicsObject::initWithFrameName(const std::string& filename)
{


	if (!Sprite::initWithSpriteFrameName(filename))
	{
		return false;
	}

	log("Box2DPhysicsObject init");

	_beginContactCallEnabled = true;
	_endContactCallEnabled = false;
	_preContactCallEnabled = false;
	_postContactCallEnabled = false;

	_world = Box2DManager::getInstance()->getWorld();

	this->createBody();
	this->createShape();
	this->createFixture();

	_body->SetUserData(this);

	return true;
}


bool Box2DPhysicsObject::init(const std::string& filename)
{


	if (!Sprite::initWithFile(filename))
	{
		return false;
	}

	log("Box2DPhysicsObject init");

	_beginContactCallEnabled = true;
	_endContactCallEnabled = false;
	_preContactCallEnabled = false;
	_postContactCallEnabled = false;

	_world = Box2DManager::getInstance()->getWorld();

	this->createBody();
	this->createShape();
	this->createFixture();

	_body->SetUserData(this);

	return true;
}

/*
void Box2DPhysicsObject::setWorld(b2World *world)
{
	

	
}*/

Box2DPhysicsObject::~Box2DPhysicsObject()
{
	log("delete box2d obejct");
}


void Box2DPhysicsObject::createBody()
{
	log("Box2DPhysicsObject defineBody");
}
void Box2DPhysicsObject::createShape()
{

}

void  Box2DPhysicsObject::createFixture()
{

}


void  Box2DPhysicsObject::handleBeginContact(b2Contact* contact)
{
	log("parent");
}


void  Box2DPhysicsObject::handleEndContact(b2Contact* contact)
{

}


void  Box2DPhysicsObject::handlePreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{

}

void  Box2DPhysicsObject::handlePostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{

}