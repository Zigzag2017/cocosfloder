#ifndef __SHADOW_LAYER_H__
#define __SHADOW_LAYER_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "GLES-Render.h"
#include "Box2DContactListener.h"
#include "ArealCreator.h"
#include "EnemyCreator.h"
#include "BulletCreator.h"

class ShadowLayer : public cocos2d::Layer
{
public:

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	//virtual void update(float dt);

	CREATE_FUNC(ShadowLayer);



};

#endif // __HELLOWORLD_SCENE_H__