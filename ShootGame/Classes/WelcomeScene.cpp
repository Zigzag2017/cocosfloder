#include "WelcomeScene.h"
#include "WelcomeReader.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* WelcomeScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = WelcomeScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

Scene* WelcomeScene::createFirstScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	CSLoader* instance = CSLoader::getInstance();//ȫ��Ψһ��zz
	instance->registReaderObject("WelcomeReader", (ObjectFactory::Instance)WelcomeReader::getInstance);//ע��Reader

	// 'layer' is an autorelease object
	auto layer = WelcomeScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool WelcomeScene::init()
{
	//**  you can create scene with following comment code instead of using csb file.
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();




	auto rootNode = CSLoader::createNode("Welcome.csb");


	addChild(rootNode);

	return true;
}
