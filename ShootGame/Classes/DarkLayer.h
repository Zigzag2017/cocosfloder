#ifndef __DARK_LAYER_H__
#define __DARK_LAYER_H__

#include "cocos2d.h"

class DarkLayer : public cocos2d::Node
{
public:
	CREATE_FUNC(DarkLayer);
	static DarkLayer* shaderNodeWithVertex(const std::string &vert, const std::string &frag);

	virtual void update(float dt) override;
	virtual void setPosition(const cocos2d::Vec2 &newPosition) override;
	virtual void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags) override;

	virtual void setHeroPosition(const cocos2d::Vec2 &newPosition, bool direction);

protected:
	DarkLayer();
	~DarkLayer();

	bool initWithVertex(const std::string &vert, const std::string &frag);
	void loadShaderVertex(const std::string &vert, const std::string &frag);

	void onDraw(const cocos2d::Mat4& transform, uint32_t flags);

	cocos2d::Vec2 _rect_left_down;//����
	cocos2d::Vec2 _rect_right_up;//����

	cocos2d::Vec2 _center;
	cocos2d::Vec2 _resolution;
	float      _time;
	std::string _vertFileName;
	std::string _fragFileName;
	cocos2d::CustomCommand _customCommand;
};


#endif //__LIGHT_LAYER_H__