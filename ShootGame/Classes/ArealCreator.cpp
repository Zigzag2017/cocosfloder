#include "ArealCreator.h"
#include "cocos2d.h"
#include "Box2DManager.h"

USING_NS_CC;

ArealCreator::ArealCreator()
{
	leftInsulateWall = nullptr;
	leftWall = nullptr;
	rightInsulateWall = nullptr;
	rightWall = nullptr;
	topWall = nullptr;
	bottomWall = nullptr;
	this->init();
}

ArealCreator::~ArealCreator()
{
	CC_SAFE_DELETE(topWallView);
	CC_SAFE_DELETE(bottomWallView);
	CC_SAFE_DELETE(leftInsulateWallView);
	CC_SAFE_DELETE(rightInsulateWallView);
	CC_SAFE_DELETE(leftWallView);
	CC_SAFE_DELETE(rightWallView);
	
}

void ArealCreator::init()
{
	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_world = Box2DManager::getInstance()->getWorld();

	bottomWall = this->createBox(visibleSize.width / 2, 0, visibleSize.width*2, 10,1);

	topWall = this->createBox(visibleSize.width / 2, visibleSize.height*2/3 , visibleSize.width*2, 10,1);

	leftInsulateWall = this->createBox(0, visibleSize.height , 10, visibleSize.height*2,2);
	rightInsulateWall = this->createBox(visibleSize.width, visibleSize.height , 10, visibleSize.height*2,2);

	leftWall = this->createBox(visibleSize.width / -2, visibleSize.height , 10, visibleSize.height*2,0);
	rightWall = this->createBox(visibleSize.width / 2 * 3, visibleSize.height , 10, visibleSize.height*2,0);

	topWallView = new BaseWall();
	topWallView->objectClass = ObjectClass::WALL_OBJECT;
	bottomWallView = new BaseWall();
	bottomWallView->objectClass = ObjectClass::WALL_OBJECT;
	leftInsulateWallView = new BaseWall();
	leftInsulateWallView->objectClass = ObjectClass::INSULATE_OBJECT;
	rightInsulateWallView = new BaseWall();
	rightInsulateWallView->objectClass = ObjectClass::INSULATE_OBJECT;
	leftWallView = new BaseWall();
	leftWallView->objectClass = ObjectClass::WALL_OBJECT;
	rightWallView = new BaseWall();
	rightWallView->objectClass = ObjectClass::WALL_OBJECT;

	topWall->SetUserData(topWallView);
	bottomWall->SetUserData(bottomWallView);
	leftInsulateWall->SetUserData(leftInsulateWallView);
	rightInsulateWall->SetUserData(rightInsulateWallView);
	leftWall->SetUserData(leftWallView);
	rightWall->SetUserData(rightWallView);

}

void ArealCreator::updatePosition(float heroDistance)
{
	bottomWall->SetTransform(b2Vec2((visibleSize.width / 2 + heroDistance) / 32.0, 0 / 32.0), 0);

	topWall->SetTransform(b2Vec2((visibleSize.width / 2 + heroDistance) / 32.0, visibleSize.height / 24.0), 0);

	leftInsulateWall->SetTransform(b2Vec2((0 + heroDistance) / 32.0, visibleSize.height  / 32.0), 0);
	rightInsulateWall->SetTransform(b2Vec2((visibleSize.width + heroDistance) / 32.0, visibleSize.height  / 32.0), 0);

	leftWall->SetTransform(b2Vec2((visibleSize.width / -2 + heroDistance) / 32.0, visibleSize.height / 32.0), 0);
	rightWall->SetTransform(b2Vec2((visibleSize.width / 2 * 3 + heroDistance) / 32.0, visibleSize.height / 32.0), 0);
}

b2Body* ArealCreator::createBox(float posX, float posY, float width, float height,int type)
{
	b2BodyDef bodyDef;
	if (type == 0)
	{
		bodyDef.type = b2BodyType::b2_dynamicBody;
	}
	else
	{
		bodyDef.type = b2BodyType::b2_kinematicBody;//������
	}
	bodyDef.position.Set(posX / 32.0, posY / 32.0);
	bodyDef.fixedRotation = true;

	b2PolygonShape shape;
	shape.SetAsBox(width / 2 / 32.0, height / 2 / 32.0);

	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = &shape;


	if (type == 2)
	{
		fixtureDef.filter.categoryBits = 0x0008;
		fixtureDef.filter.maskBits = 0x0009;
	}
	else
	{
		fixtureDef.filter.groupIndex = 1;
	}


	auto wall = _world->CreateBody(&bodyDef);
	wall->CreateFixture(&fixtureDef);

	return wall;

}