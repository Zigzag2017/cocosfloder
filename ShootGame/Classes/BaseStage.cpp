#include "BaseStage.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "VirtualHandle.h"
#include <math.h>
#include "Hero.h"
#include "Box2DManager.h"
#include "NormalEnemy.h"
#include "MainBullet.h"
#include "FiniteStateMachine.h"
#include "EnemyCreator.h"
#include "ItemCreator.h"
#include "GameManager.h"
#include "BaseParticle.h"
#include "BaseItem.h"
#include "BackgroundManager.h"
#include "LineBullet.h"
#include "WelcomeScene.h"

USING_NS_CC;

//**************************左右镜头缓动，身体位置确立清楚

using namespace cocostudio::timeline;

Scene* BaseStage::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = BaseStage::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

/*
*英雄不被操控时不应该被推动
*
*场景管理，操控敌人出现和镜头范围
*
*
*/

// on "init" you need to initialize your instance
bool BaseStage::init()
{
	//**  you can create scene with following comment code instead of using csb file.
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	btn1 = false;
	btn2 = false;
	btn3 = false;
	btn4 = false;
	killNum = 0;
	flag1 = false;
	flag2 = false;





	backgroundManager = BackgroundManager::create();//记得从堆里删除
	backgroundManager->retain();
	//bg setup ↓

	//backgroundManager->setVisible(false);

	shadowLayer = ShadowLayer::create();
	
	objectLayer = Layer::create();

	//darkLayer = DarkLayer::shaderNodeWithVertex("light/dark.vsh", "light/dark.fsh");
	//darkLayer->setVisible(false);
	darkPic = Sprite::create("light/pic.png");
	darkPic->setVisible(false);
	darkPic->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));

	d = Sprite::create("light/pic.png");
	
	d->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));

	lightLayer = LightLayer::shaderNodeWithVertex("light/sea.vsh", "light/sea.fsh");


	lightLayer->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));

	
	
	this->addChild(backgroundManager);
	this->addChild(shadowLayer);
	this->addChild(darkPic);
	this->addChild(lightLayer);
	this->addChild(objectLayer);
	this->addChild(d);
	

	Box2DManager::getInstance()->init();/////////////////////////场景消失时销毁
	_world = Box2DManager::getInstance()->getWorld();

	particleCreator = new ParticleCreator(8, 1);
	particleCreator->setStage(this);
	//particleCreator->makeParticles(10,100,500,400,1);


	hero = Hero::create();

	hero->setStage(this);

	hero->setPosition(Vec2(visibleSize.width / 3, visibleSize.height / 2));


	this->objectLayer->addChild(hero);

	//hero->setWorld(_world);

	_hero = hero->_body;


	lineBullet = LineBullet::create(visibleSize.width / 9 * 7, visibleSize.height * 4 / 5,0);
	lineBullet->retain();//***********记得删除
	lineBullet->setStage(this);
	hero->lineBullet = lineBullet;
	this->objectLayer->addChild(lineBullet);


	backgroundManager->setup(hero,this);//bg setup

	enemyCreator = new EnemyCreator();//记得从堆删除
	//根据关卡AI匹配
	//enemyCreator->addNormalEnemy(this, visibleSize.width / 9 * 6, visibleSize.height / 2);
	
	enemyCreator->addNormalEnemy(this, visibleSize.width / 9 * 8, visibleSize.height / 2);


	//itemCreator->addItem(this, visibleSize.width / 9 * 7, visibleSize.height * 4 / 5, 0);
	

	arealCreator = new ArealCreator();//记得从堆删除

	bulletCreator = new BulletCreator(8, 1);

	gameManager = GameManager::create();//记得从堆里删除
	gameManager->retain();
	this->addChild(gameManager);
	//英雄索引，更新坐标
	//区域管理者索引，更新位置
	gameManager->setup(hero, arealCreator);


	//调试视图
	_debugDraw = new GLESDebugDraw(32);

	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	//flags += b2Draw::e_jointBit;
	//flags += b2Draw::e_aabbBit;
	//flags += b2Draw::e_pairBit;
	//flags += b2Draw::e_centerOfMassBit;

	//_debugDraw->SetFlags(flags);
	//_world->SetDebugDraw(_debugDraw);

	InitUI();//血条等UI


	auto v = VirtualHandle::create();
	
	v->setFireEvent(leftBtn,rightBtn);

	v->setPositionY(visibleSize.height / 6);
	v->setTag(333);
	addChild(v);

	/*
	hero->runAction(Sequence::create(DelayTime::create(5), CallFunc::create([this]()
	{
	auto l = this->hero;

	//this->hero->setPosition(Vec2(200, 200));
	}
	), NULL));*/

	v->setCallback([this](VirtualHandleEvent event, float data)
	{
		std::string value;
		Hero* hero = this->hero;
		switch (event)
		{
		//case VirtualHandleEvent::BOTTOM_ATTACK_BTN_DWON:
		//	//bulletCreator->request()->used(this->_hero->GetPosition(), 0, this);//发射子弹
		//	//log("LEFT_DOWN");
		//	hero->getFsm()->execute(MessageEnum::FIRE);
		//	hero->dirLock = true;
		//	break;

		//case VirtualHandleEvent::BOTTOM_ATTACK_BTN_UP:
		//	//log("LEFT_UP");
		//	hero->dirLock = false;
		//	break;
		case VirtualHandleEvent::LEFT_ATTACK_BTN_DWON:
			//hero->getFsm()->execute(MessageEnum::SKILL);

			

			//让英雄激光弹自行消退，没有就关闭激光
			
			break;

		case VirtualHandleEvent::LEFT_ATTACK_BTN_UP:
			if(hero->getFsm()->currentState == MessageEnum::SKILL)
				hero->getFsm()->execute(MessageEnum::IDLE);
			//hero->lineBullet->stop();
			//->dirLock = false;

			break;

		case VirtualHandleEvent::RIGHT_ATTACK_BTN_DWON:


			hero->getFsm()->execute(MessageEnum::FIRE);
			hero->dirLock = true;
			break;

		case VirtualHandleEvent::RIGHT_ATTACK_BTN_UP:
			hero->dirLock = false;
			break;

		case VirtualHandleEvent::TOUCH_MOVE:
			//			hero->startMove(data);//移动角度
			//			hero->getFsm()->execute(MessageEnum::MOVE);//让fsm处理

			//封装移动函数，判断是否可以操控
			//this->_hero->SetLinearVelocity(b2Vec2(cos(data) * 10, sin(data) * 10));
			hero->startMove(data);
			break;
		case VirtualHandleEvent::END_MOVE:
			//			hero->getFsm()->execute(MessageEnum::IDLE);
			hero->endMove();
			//this->_hero->SetLinearVelocity(b2Vec2(0, 0));
			break;
		default:
			break;
		}

	});


	EventListenerKeyboard* keyboardListner = EventListenerKeyboard::create();

	keyboardListner->onKeyPressed = [&](EventKeyboard::KeyCode keycode, Event* event) {
		switch (keycode) {
		case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			CCLOG("LEFT");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			CCLOG("RIGHT");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
			CCLOG("UP");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			CCLOG("DOWN");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_1:
			hero->getFsm()->execute(MessageEnum::SKILL);
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_2:
			hero->getFsm()->execute(MessageEnum::FIRE);
			hero->dirLock = true;
			break;
		default:
			break;
		}
	};
	keyboardListner->onKeyReleased = [&](EventKeyboard::KeyCode keycode, Event* event) {
		switch (keycode) {
		case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			CCLOG("LEFT");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			CCLOG("RIGHT");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
			CCLOG("UP");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			CCLOG("DOWN");
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_1:
			if (hero->getFsm()->currentState == MessageEnum::SKILL)
				hero->getFsm()->execute(MessageEnum::IDLE);
			break;
		case cocos2d::EventKeyboard::KeyCode::KEY_2:
			hero->dirLock = false;
			break;
		default:
			break;
		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListner, this);


	this->scheduleUpdate();




	//auto ss = Sprite::create("CloseNormal.png");
	//this->addChild(ss);
	//auto seq = Sequence::create(DelayTime::create(5), CallFuncN::create(CC_CALLBACK_1(BaseStage::test, this)), NULL);
	//ss->runAction(seq);


	//l = Label::create();
	//l->setScale(3);
	//l->setPosition(visibleSize/2);
	//this->addChild(l);

	return true;
}

void BaseStage::test(cocos2d::Ref* sender)
{
	//this->gameManager->darkness();
	if (!flag1)
	{
		auto visibleSize = Director::getInstance()->getVisibleSize();
	flag1 = true;
	shadowLayer->setVisible(false);
	lightLayer->setVisible(false);
	backgroundManager->setVisible(false);
	/*objectLayer->setVisible(false);*/
	darkPic->setVisible(true);
	enemyCreator->addBoss(this, 5500, visibleSize.height / 2);
	auto seq = Sequence::create(DelayTime::create(1), CallFuncN::create(CC_CALLBACK_1(BaseStage::test, this)), NULL);
	this->runAction(seq);
	}
	else
	{
		flag2 = true;
	}
	//darkLayer->setOpacity(212);
}

void  BaseStage::InitUI()
{
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("hud/Hud.plist");
	//加载UI界面
	auto rootNode = CSLoader::createNode("hud/HUD.csb");
	rootNode->setTag(444);
	addChild(rootNode);



	//进行界面适配
	auto visibleSize = Director::getInstance()->getVisibleSize();
	rootNode->setContentSize(visibleSize);
	ui::Helper::doLayout(rootNode);
	cocos2d::ui::Layout* rootPane = static_cast<cocos2d::ui::Layout*>(rootNode->getChildByName("Panel_1"));
	//获取控件
	auto Button_1 = static_cast<cocos2d::ui::Button*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Button_3"));
	//给设置按钮标记，已区分点击的是那个按钮
	Button_1->setTag(1);
	//添加按钮点击事件
	Button_1->addClickEventListener(CC_CALLBACK_1(BaseStage::onButtonClick, this));
	//获取控件
	leftBtn = static_cast<cocos2d::ui::Button*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Button_left"));
	//给设置按钮标记，已区分点击的是那个按钮
	leftBtn->setTag(2);
	//获取控件
	rightBtn = static_cast<cocos2d::ui::Button*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Button_right"));
	//给设置按钮标记，已区分点击的是那个按钮
	rightBtn->setTag(3);
	//添加按钮点击事件
	//Button_2->addClickEventListener(CC_CALLBACK_1(HelloWorld::onButtonClick, this));


	auto hp = static_cast<cocos2d::ui::ImageView*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Image_hp"));
	//给设置按钮标记，已区分点击的是那个按钮
	hp->setTag(4);

	auto mp = static_cast<cocos2d::ui::ImageView*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Image_mp"));
	//给设置按钮标记，已区分点击的是那个按钮
	mp->setTag(5);

	auto big_mp = static_cast<cocos2d::ui::ImageView*>(cocos2d::ui::Helper::seekWidgetByName(rootPane, "Image_big_mp"));
	//给设置按钮标记，已区分点击的是那个按钮
	big_mp->setTag(6);

	hero->setImage(hp,mp,big_mp);

}
void BaseStage::onButtonClick(cocos2d::Ref* sender)
{
	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5, WelcomeScene::createScene()));
}

b2Body* BaseStage::createBox(float posX, float posY, float width, float height)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_staticBody;
	bodyDef.position.Set(posX / 32.0, posY / 32.0);
	bodyDef.fixedRotation = true;

	b2PolygonShape shape;
	shape.SetAsBox(width / 2 / 32.0, height / 2 / 32.0);

	b2FixtureDef fixtureDef;
	fixtureDef.density = 100;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = &shape;


	auto box = _world->CreateBody(&bodyDef);
	box->CreateFixture(&fixtureDef);

	return box;

}

b2Body* BaseStage::createObjet(float posX, float posY, float radius)
{

	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(posX / 32.0, posY / 32.0);
	bodyDef.fixedRotation = true;
	//bodyDef.linearDamping = 1;

	b2CircleShape shape;
	shape.m_radius = radius / 32.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.density = 100;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = &shape;

	auto object = _world->CreateBody(&bodyDef);
	object->CreateFixture(&fixtureDef);

	return object;
}

void BaseStage::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
	GL::enableVertexAttribs(cocos2d::GL::VERTEX_ATTRIB_FLAG_POSITION);

	_world->DrawDebugData();

	CHECK_GL_ERROR_DEBUG();
}

void BaseStage::update(float dt)
{
	_world->Step(1.0f / 60.0f, 10, 10);

	//floor->setAnchorPoint(Vec2(0,0));
	backgroundManager->updatePosition(-1.0 * gameManager->currentPoint);
	//backgroundManager->setPositionX(-1*gameManager->currentPoint);

	d->setVisible(false);

	if (killNum == 1 && gameManager->currentPoint > 800 && !btn1)
	{
		btn1 = true;
		gameManager->beginRect = 960;
		gameManager->endRect = 960 * 3;

		auto visibleSize = Director::getInstance()->getVisibleSize();
		enemyCreator->addNormalEnemy(this, 2000 , visibleSize.height / 2+50);
		enemyCreator->addNormalEnemy(this, 1900 , visibleSize.height / 2);
		enemyCreator->addNormalEnemy(this, 1800 , visibleSize.height / 2-50);

		itemCreator->addItem(this, 1900, visibleSize.height * 4 / 5, 1);
		//beginRect = 0;
		//endRect = 960 * 2;
	}
	else if (killNum == 4 && gameManager->currentPoint > 1800 && !btn2)
	{

		btn2 = true;
		gameManager->beginRect = 960 * 2;
		gameManager->endRect = 960 * 6;

		auto visibleSize = Director::getInstance()->getVisibleSize();
		enemyCreator->addNormalEnemy(this, 2800, visibleSize.height / 2);
		enemyCreator->addNormalEnemy(this, 2900, visibleSize.height / 2 - 50);
		enemyCreator->addNormalEnemy(this, 3000, visibleSize.height / 2 - 50);
		enemyCreator->addNormalEnemy(this, 2900, visibleSize.height / 2 + 50);
		enemyCreator->addNormalEnemy(this, 3000, visibleSize.height / 2 + 50);
		itemCreator->addItem(this, 2800, visibleSize.height * 4 / 5, 1);
		itemCreator->addItem(this, 3100, visibleSize.height * 4 / 5, 2);
		//itemCreator->addItem(this, 3100, visibleSize.height * 4 / 5, 3);

	}
	else if (killNum == 9 && gameManager->currentPoint > 4700 && !btn3)
	{
		btn3 = true;
		gameManager->beginRect = 960 * 5;
		gameManager->endRect = 960 * 6;

		auto visibleSize = Director::getInstance()->getVisibleSize();
		
		//this->getChildByTag(333)->setVisible(false);
		//this->getChildByTag(444)->setVisible(false);
		//l->setVisible(false);
		//gameManager->darkness();
		//auto visibleSize = Director::getInstance()->getVisibleSize();
		//flag1 = true;
		
		auto seq = Sequence::create(DelayTime::create(2), CallFuncN::create(CC_CALLBACK_1(BaseStage::test, this)), NULL);
		this->runAction(seq);
	}
	else if (killNum == 9 && gameManager->currentPoint > 4000 && !btn4)
	{
		btn4 = true;
		auto visibleSize = Director::getInstance()->getVisibleSize();
		itemCreator->addItem(this, 5500, visibleSize.height * 1 / 5+100, 3);
	}
	

	//char t[64];
	//sprintf(t, "%d", int(gameManager->currentPoint));
	//std::string a(t);
	//std::string b = "  currentPoint  :";
	//std::string c = b+a;



	////log("%d",(int)_hero->GetPosition().x*32);

	//l->setString(c  );
}
