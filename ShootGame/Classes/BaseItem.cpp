#include "BaseItem.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"
#include "GameManager.h"
#include "Hero.h"

USING_NS_CC;



BaseItem::BaseItem(float x, float y, int type)
{
	_x = x;
	_y = y;
	isDeath = false;
	doDeath = false;
	log("run BaseItem -> yes");
	btn = false;
	this->type = type;

	fHeight = 0.0;//浮动 高度
	fCount = 0.0;

}

void BaseItem::createFixture()
{
	log("BaseEnemy createFixture");

	//bodyDef.linearDamping = 1;

	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.1;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;
	fixtureDef.userData = this;//身体储存对象，hit没有东西
	fixtureDef.filter.categoryBits = 0x0004;//ObjectBits::ITEM;
	fixtureDef.filter.maskBits = 0x0015;
	if (type == 3)
	{
		fixtureDef.isSensor = true;
	}
	//fixtureDef.filter.groupIndex = 1;
	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);

}

void BaseItem::createShape()
{
	log("BaseEnemy createShape");

	b2CircleShape *shape = new b2CircleShape();
	shape->m_radius = 24.0f / 32.0f;
	_shape = shape;

}

void BaseItem::createBody()
{
	log("BaseEnemy createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(_x / 32.0, _y / 32.0);
	bodyDef.fixedRotation = true;
	bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
}



void BaseItem::handleBeginContact(b2Contact* contact)
{
	log("item and hero contact!!!");
	Box2DPhysicsObject* other = (this == contact->GetFixtureA()->GetBody()->GetUserData()) ? (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData() : (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
	if (other->objectClass == ObjectClass::HERO_OBJECT)
	{
		
		//log("catch");
		if (this->isDeath == false)
		{
			if (type == 1)
			{
				isDeath = true;
				stage->hero->lifes = 100;
			}
			else if (type == 2)
			{
				isDeath = true;
				stage->hero->lines = 600;
				stage->leftBtn->setEnabled(true);
			}
			
			//this->death();
		}
	}

}

void BaseItem::death()
{
	log("item used...................");
	//_world->DestroyBody(_body);
	stage->shadowLayer->removeChild(shadow);
	stage->objectLayer->removeChild(this);
	
	
}

BaseItem::~BaseItem()
{
	log("delete item");
	_world->DestroyBody(_body);
}

void BaseItem::handleEndContact(b2Contact* contact)
{
	//log("contact!!!");
}


bool BaseItem::init(int type)//道具类型~~~~~~~~~~~~~~~~~~~~~~
{
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("player/Player.plist");
	std::string filename = "P/items_life.png";
	if (type == 1)filename = "P/items_life.png";
	else if (type == 2)filename = "P/items_power.png";
	else if (type == 3)filename = "P/box_close.png";
	else if (type == 4)filename = "P/box_open.png";

	if (!Box2DPhysicsObject::initWithFrameName(filename))
	{
		return false;
	}

	shadow = Sprite::createWithSpriteFrameName("P/shadow_items.png");
	shadow->setAnchorPoint(Vec2(0.5, 0.5));

	objectClass = ObjectClass::ITEM_OBJECT;

	scheduleUpdate();//启动定时器

	return true;
}

void BaseItem::update(float d)
{
	
	fCount += 0.04;
	fHeight = 10 * cos(fCount);
	if (type == 3){
		shadow->setVisible(false);
		fHeight = 20 * cos(fCount);
	}

	if (stage->flag1&&!btn)
	{
		btn = true;
		
		this->setSpriteFrame(Sprite::createWithSpriteFrameName("P/box_open.png")->getSpriteFrame());
		
	}
	if (stage->flag2)
	{
		isDeath = true;
	}

		//道具状态机
		b2Vec2 position = _body->GetPosition();



		this->setPositionX(position.x * 32.0 - this->stage->gameManager->currentPoint);
		this->setPositionY(position.y * 5.536 + 40 + 150 + fHeight);




		shadow->setPositionX(position.x * 32.0 - this->stage->gameManager->currentPoint);
		shadow->setPositionY(position.y * 5.536 + 150);
		//shadow->setScaleY(0.3);

		this->setZOrder(1000 - (int)position.y);

	
	if (this->isDeath&&!this->doDeath)
	{
		this->doDeath = true;
		death();
	}

}

void BaseItem::setStage(BaseStage* s)
{
	this->stage = s;
	s->shadowLayer->addChild(shadow);
}
