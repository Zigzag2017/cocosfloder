#ifndef __FIRE_STATE_H_  
#define __FIRE_STATE_H_  
#include "State.h"  
#include "cocos2d.h"

USING_NS_CC;
class FireState :public State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum);
	virtual void run(Hero *hero);
	virtual void fire(Node *node);
};

#endif  