#include "VirtualHandle.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <math.h>

USING_NS_CC;

using namespace cocostudio::timeline;

VirtualHandle::VirtualHandle() :
_rockerRange(nullptr),
_rocker(nullptr),
_right_attack(nullptr),
_left_attack(nullptr),
_rockerRangeValue(204),
_rockerTouchID(-1),
_rockerWay(0),
_rockerLastPointX(0),
_rockerLastPointY(0)
{

}

VirtualHandle::~VirtualHandle()
{



}

void VirtualHandle::setFireEvent(ui::Button *left, ui::Button *right)
{
	_left_attack = left;
	_right_attack = right;

	left->addTouchEventListener(std::bind(&VirtualHandle::touchEvent, this,
		std::placeholders::_1, std::placeholders::_2));

	right->addTouchEventListener(std::bind(&VirtualHandle::touchEvent, this,
		std::placeholders::_1, std::placeholders::_2));
}

bool VirtualHandle::init()
{
	auto size = Director::getInstance()->getVisibleSize();
	
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("hud/Hud.plist");
	_rockerRange = ui::Button::create("hud/move.png", "HUD/move.png");

	_rocker = Sprite::create("hud/move_ctrl.png");
	
	//_rocker = ui::Button::create("btn.png");
	//_left_attack = ui::Button::create("btn.png", "btn_down.png");
	//_right_attack = ui::Button::create("btn.png", "btn_down.png");
	//_bottom_attack = ui::Button::create("btn.png", "btn_down.png");

	//	_rockerRange->setContentSize(Size(_rockerRangeValue, _rockerRangeValue/*_rocker->
	//		getContentSize().height*/));

	_rockerRange->setPosition(Vec2(_rockerRangeValue / 2, -13));
	
	//_bottom_attack->setPosition(Vec2(size.width - _right_attack->getContentSize().width, 0));
	//_left_attack->setPosition(Vec2(size.width - _right_attack->getContentSize().width*2, _right_attack->getContentSize().height + 20));
	//_right_attack->setPosition(Vec2(size.width - _right_attack->getContentSize().width, _right_attack->getContentSize().height+20));
	/**/
	/*
	auto sp = cocos2d::Sprite::create("HelloWorld.png");
	sp->setColor(Color3B::BLACK);
	sp->setAnchorPoint(_rockerRange->getAnchorPoint());
	sp->setPosition(_rockerRange->getPosition());
	sp->setContentSize(Size(_rockerRangeValue, _rockerRangeValue));*/
	//sp->
	//	addChild(sp);
	/**/

	_rocker->setPosition(Vec2(_rockerRangeValue / 2, _rockerRangeValue / 2 /*_rocker->getContentSize().height/2*/));
	//_rocker->set

	//log("init:********%f***%f", _rocker->getPositionX(), _rocker->getPositionY());

	_rockerRange->addChild(_rocker);
	_rockerRange->setTouchEnabled(false);
	addChild(_rockerRange);
	//addChild(_right_attack);
	//addChild(_left_attack);
	//addChild(_bottom_attack);

	//_left_attack->addTouchEventListener(std::bind(&VirtualHandle::touchEvent, this,
	//	std::placeholders::_1, std::placeholders::_2));

	//_right_attack->addTouchEventListener(std::bind(&VirtualHandle::touchEvent, this,
	//	std::placeholders::_1, std::placeholders::_2));

	//_bottom_attack->addTouchEventListener(std::bind(&VirtualHandle::touchEvent, this,
	//	std::placeholders::_1, std::placeholders::_2));

	//log("3776*******%f,%f,%f", _rockerRange->getPositionY(), _rockerRange->getContentSize().height, _rockerRange->getAnchorPoint().y);

	auto event = Director::getInstance()->getEventDispatcher();

	auto rockerRangeEvent = EventListenerTouchOneByOne::create();
	rockerRangeEvent->onTouchBegan = [this](Touch* touch, Event* e)
	{

		//log("began");

		auto bound = _rockerRange->getBoundingBox();
		bound.origin = _rockerRange->convertToWorldSpace(Vec2(0, 0));

		auto point = touch->getLocation();

		if (bound.containsPoint(point))
		{

			//log("true");

			_rockerTouchID = touch->getID();
			_rockerLastPointX = point.x;
			_rockerLastPointY = point.y;

			//auto point = touch->getLocation();

			float d;



			if (point.x - 102 != 0)
			{

				if (point.x - 102 > 0)
					d = atan((point.y-13 - this->getPosition().y) / (point.x - 102));
				else
				{
					d = atan((point.y-13 - this->getPosition().y) / (point.x - 102)) + 3.14;
				}
			}
			else
			{
				if (point.y-13 > this->getPosition().y)
					d = 1.57;
				else
					d = 4.71;
			}
			callback(VirtualHandleEvent::TOUCH_MOVE, d);

			return true;
		}


		//是否有效，能否继续
		return false;
	};

	rockerRangeEvent->onTouchMoved = [this](Touch* touch, Event* e)
	{
		if (_rockerTouchID == -1 || _rockerTouchID != touch->getID())
		{
			return;
		}

		auto point = touch->getLocation();
		/*
		if (fabsf(_rockerLastPointX - point.x) < 20)
		{
		return;
		}
		if (fabsf(_rockerLastPointY - point.y) < 20)
		{
		return;
		}
		*/
		float d;



		if (point.x - 150 != 0)
		{

			if (point.x - 150 > 0)
				d = atan((point.y-13 - this->getPosition().y) / (point.x - 150));
			else
			{
				d = atan((point.y-13 - this->getPosition().y) / (point.x - 150)) + 3.14;
			}
		}
		else
		{
			if (point.y-13 > this->getPosition().y)
				d = 1.57;
			else
				d = 4.71;
		}
		//log("***************%f", (point.y - this->getPosition().y) / (point.x - 150));
		callback(VirtualHandleEvent::TOUCH_MOVE, d);
		//log("change_move***%.0f***%.0f***",point.x,point.y);

		updateRockerPos(point);

	};

	rockerRangeEvent->onTouchEnded = [this](Touch* touch, Event* e)
	{
		if (_rockerTouchID == -1 || _rockerTouchID != touch->getID())
		{
			return;
		}
		cancelWay();
		callback(VirtualHandleEvent::END_MOVE, 0);
		_rockerTouchID = -1;
		_rockerLastPointX = 0;
		_rockerLastPointY = 0;
		updateRockerPos(Vec2(_rockerRangeValue / 2, _rockerRangeValue / 2 - 13 + this->getPosition().y - _rockerRange->getContentSize().height* _rockerRange->getAnchorPoint().y));
	};

	rockerRangeEvent->onTouchCancelled = [this](Touch* touch, Event* e)
	{
		if (_rockerTouchID == -1 || _rockerTouchID != touch->getID())
		{
			return;
		}
		cancelWay();
		callback(VirtualHandleEvent::END_MOVE, 0);
		_rockerTouchID = -1;
		_rockerLastPointX = 0;
		_rockerLastPointY = 0;
		updateRockerPos(Vec2(_rockerRangeValue / 2, _rockerRangeValue / 2 - 13 + this->getPosition().y - _rockerRange->getContentSize().height* _rockerRange->getAnchorPoint().y));
	};


	event->addEventListenerWithSceneGraphPriority(rockerRangeEvent, _rockerRange);


	return true;
}


void VirtualHandle::touchEvent(Ref *obj, ui::Widget::TouchEventType type)
{
	//if (obj == this)log("-------------------------self");

	switch (type){
	case ui::Widget::TouchEventType::BEGAN:
		if (obj == _right_attack)
		{
			callback(VirtualHandleEvent::RIGHT_ATTACK_BTN_DWON, 0);
		}
		else if (obj == _left_attack)
		{
			callback(VirtualHandleEvent::LEFT_ATTACK_BTN_DWON, 0);
		}
		else if (obj == _bottom_attack)
		{
			callback(VirtualHandleEvent::BOTTOM_ATTACK_BTN_DWON, 0);
		}
		break;

	case ui::Widget::TouchEventType::ENDED:
		if (obj == _right_attack)
		{
			callback(VirtualHandleEvent::RIGHT_ATTACK_BTN_UP, 0);
		}
		else if (obj == _left_attack)
		{
			callback(VirtualHandleEvent::LEFT_ATTACK_BTN_UP, 0);
		}
		else if (obj == _bottom_attack)
		{
			callback(VirtualHandleEvent::BOTTOM_ATTACK_BTN_UP, 0);
		}
		break;

	case ui::Widget::TouchEventType::CANCELED:
		if (obj == _right_attack)
		{
			callback(VirtualHandleEvent::RIGHT_ATTACK_BTN_UP, 0);
		}
		else if (obj == _left_attack)
		{
			callback(VirtualHandleEvent::LEFT_ATTACK_BTN_UP, 0);
		}
		else if (obj == _bottom_attack)
		{
			callback(VirtualHandleEvent::BOTTOM_ATTACK_BTN_UP, 0);
		}
		break;

	default:
		break;

	}

}

void VirtualHandle::callback(VirtualHandleEvent event, float data)
{
	if (_callback != nullptr)
	{
		//char *s = "zz";
		_callback(event, data);
	}
}

void VirtualHandle::setCallback(std::function<void(VirtualHandleEvent, float)> callback)
{
	_callback = callback;

}

void VirtualHandle::updateRockerPos(Vec2 position)
{
	auto valueX = _rockerRange->convertToNodeSpace(position).x;

	if (valueX < 50)
	{
		valueX = 50;
	}
	if (valueX>_rockerRangeValue - 50)
	{
		valueX = _rockerRangeValue - 50;
	}

	/*
	log("vec1*******%f,%f", position.x, position.y);
	log("vec2*******%f,%f", convertToNodeSpace(position).x, convertToNodeSpace(position).y);
	log("vec3*******%f,%f", _rockerRange->getContentSize().width, _rockerRange->getContentSize().height);//块的大小
	log("vec4*******%f,%f", _rockerRange->getAnchorPoint().x, _rockerRange->getAnchorPoint().y);//锚点
	log("vec6*******%f,%f", _rockerRange->getPosition().x, _rockerRange->getPosition().y);//摇杆块相对层位置
	log("vec7*******%f,%f", this->getPosition().x, this->getPosition().y);//整个控制层位置
	log("vec8*******%f,%f", this->getAnchorPoint().y, this->getContentSize().height);//
	*/

	auto valueY = _rockerRange->convertToNodeSpace(position).y;

	if (valueY < 50)
	{
		valueY = 50;
	}
	if (valueY>_rockerRangeValue - 50)
	{
		valueY = _rockerRangeValue - 50;
	}
	/*
	valueX += this->getPosition().x;
	valueY += this->getPosition().y - _rockerRange->getContentSize().height*_rockerRange->getAnchorPoint().y;
	*/
	//log("**************************%f***%f", valueX, valueY);
	//_rocker->stopAllActions();
	_rocker->setPosition(Vec2(valueX, valueY));
	//_rocker->runAction(MoveTo::create(0.03, Vec2(valueX, valueY)));

}

void VirtualHandle::cancelWay()
{
	//log("cancel_move");
	//auto s = Sprite::create();
	//this->addChild(s);
}