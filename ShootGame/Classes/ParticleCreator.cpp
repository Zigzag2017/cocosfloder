#include "ParticleCreator.h"
#include "BaseParticle.h"
#include "ui/CocosGUI.h"
#include "BaseStage.h"
#include <math.h>

USING_NS_CC;

ParticleCreator::ParticleCreator(int initSize, int style)
{
	m_initSize = initSize;
	allocate(initSize);
}

ParticleCreator::~ParticleCreator()
{


}

BaseParticle* ParticleCreator::request()
{
	if (m_freeList.size() == 0)
	{
		allocate(m_initSize / 2);
	}

	BaseParticle* particle = *m_freeList.begin();

	m_freeList.erase(m_freeList.begin());
	m_usedList.pushBack(particle);

	log("free %d  used %d", m_freeList.size(), m_usedList.size());

	return particle;
}

void ParticleCreator::putback(BaseParticle* particle)
{
	for (int i = 0; i < m_usedList.size(); ++i)
	{
		if (m_usedList.at(i) == particle)
		{
			m_usedList.erase(m_usedList.begin() + i);
			m_freeList.pushBack(particle);
		}
	}
}
void ParticleCreator::setStage(BaseStage* s){ stage = s; };

void ParticleCreator::makeParticles(int targetZOrder, float x, float y, int bottom, int xSpeed)
{

	//BaseParticle* p = BaseParticle::create("s1.png", 1);
	BaseParticle* p;
	for (int i = 0; i < 5; i++)
	{
		p = request();
		p->setPosition(Vec2(100, 500));
		//this->addChild(p);
		p->used(this, targetZOrder, x, y, bottom, xSpeed*(i + 5), 0, stage);
	}
	
}


/**
*	回收多余长度
*/
void ParticleCreator::garbage()
{
	if (m_freeList.size() > m_initSize)
	{
		Vector<BaseParticle*>::iterator it = m_freeList.begin();
		int pIndex = 0;

		while (it != m_freeList.end())
		{
			if (pIndex > m_initSize)
			{
				BaseParticle* p = (*it);
				m_freeList.erase(it);
				freeParticle(p);
				--it;
			}
			++pIndex;
			++it;
		}
	}
}

void ParticleCreator::allocate(int size)
{
	for (int i = 0; i < size; ++i)//size%5+1
	{
		BaseParticle* p = BaseParticle::create(StringUtils::format("s%d.png", i % 5 + 1), 1);
		//BaseParticle* p = new BaseParticle(m_style);
		m_freeList.pushBack(p);
	}
}

void ParticleCreator::freeParticle(void *particle)
{
	log(" after delete ");


	BaseParticle* b = (BaseParticle*)particle;
	delete b;
}