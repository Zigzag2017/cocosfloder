#ifndef __BASE_PARTICLE_H__
#define __BASE_PARTICLE_H__

#include "cocos2d.h"
#include "BaseStage.h"

class ParticleCreator;
class BaseParticle :
	public Sprite
{
public:

	BaseParticle(int style);
	//~BaseParticle();
	static BaseParticle* create(const std::string& filename,int style)
	{
		SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("player/Player.plist");
		BaseParticle *sprite = new (std::nothrow) BaseParticle(style);
		if (sprite && sprite->initWithSpriteFrameName("P/star.png"))//createWithSpriteFrameName("P/bullet.png");//initWithFile(filename))
		{
			//sprite->autorelease();//记得回收
			return sprite;
		}
		CC_SAFE_DELETE(sprite);
		return nullptr;
	};

	BaseStage* stage;

	float m_width;
	float m_height;

	float m_x;
	float m_y;

	float m_bottom;
	float m_xSpeed;
	float m_ySpeed;//                

	float lt;//速度差。+5和-5，一个是着地，一个是0之后
	float lt_c;//速度差增减参数，随机

	float m_rotation;
	//float m_g;
	/*
	*层级，起点，水平速度，自转系数，重力加速度
	*
	*/
	ParticleCreator *creator;
	//only
	virtual void used(ParticleCreator *pc, int targetZOrder, float x, float y, int bottom, int xSpeed, float rotation, Node* stage);
	virtual void putback();



private:

	virtual void update(float d);


	int m_style;


};


#endif