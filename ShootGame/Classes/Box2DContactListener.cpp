#include "Box2DContactListener.h"
#include "Box2DPhysicsObject.h"
#include "cocos2d.h"

USING_NS_CC;

void Box2DContactListener::BeginContact(b2Contact* contact)
{
	//cocos2d::log("contact begin"); //contact->GetFixtureA

	Box2DPhysicsObject* a = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
	Box2DPhysicsObject* b = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();

	//_contactGetWorldManifoldValues(contact);

	if (a->_beginContactCallEnabled)
		a->handleBeginContact(contact);

	if (b->_beginContactCallEnabled)
		b->handleBeginContact(contact);
}


void Box2DContactListener::EndContact(b2Contact* contact)
{
	//cocos2d::log("contact end");

	Box2DPhysicsObject* a = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
	Box2DPhysicsObject* b = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();

	//_contactGetWorldManifoldValues(contact);

	if (a->_endContactCallEnabled)
		a->handleEndContact(contact);

	if (b->_endContactCallEnabled)
		b->handleEndContact(contact);
	
}

void Box2DContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	//cocos2d::log("can hit");
}

void Box2DContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{

}