#ifndef __IDLE_STATE_H_  
#define __IDLE_STATE_H_  
#include "State.h"  
class IdleState :public State
{
public:
	virtual void execute(Hero *hero, MessageEnum messageEnum);
	virtual void run(Hero *hero);
};

#endif  