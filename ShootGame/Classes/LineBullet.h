#ifndef __LINE_BULLET_H__
#define __LINE_BULLET_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Box2DPhysicsObject.h"


USING_NS_CC;

class BaseStage;
class ShaderNode;

class LineBullet :
	public Box2DPhysicsObject
{
public:
	LineBullet(float x, float y, int type);
	virtual bool init(int  type);
	~LineBullet();

	static LineBullet* create(float x, float y, int  type)
	{
		LineBullet *pRet = new LineBullet(x, y, type);
		if (pRet && pRet->init(type))
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = nullptr;
			return nullptr;
		}
	};




	virtual void createFixture();
	virtual void createShape();
	virtual void createBody();

	virtual void handleBeginContact(b2Contact* contact);
	virtual void handleEndContact(b2Contact* contact);

	bool isSetup;//是否启动

	void start();//开始运作
	void stop();//停止运作

	
	void death();


	ShaderNode* lightning;//电光层

	BaseStage* stage;
	void setStage(BaseStage* s);

	Vector<Box2DPhysicsObject*> m_catchList;//捕获敌人列表
	std::vector<Vec2> m_electricList;//闪电粒子数组
	CCParticleSystem* m_particleSystem;//闪电粒子系统
	Sprite* particleLayer;//闪电粒子系统

	int particleIndex;//闪电粒子索引，逐帧分配

	void drawElectricity();

	static bool  sort_by_distance(Box2DPhysicsObject* obj1, Box2DPhysicsObject* obj2)//对捕获敌人列表进行排序，制作闪电数组
	{
		return obj1->getPositionX() < obj2->getPositionX();
	}
private:

	virtual void update(float d);

	int identifier;//唯一标识符

	float moveAngle;//移动角度

	



};



class ShaderNode : public cocos2d::Node
{
public:
	CREATE_FUNC(ShaderNode);
	static ShaderNode* shaderNodeWithVertex(const std::string &vert, const std::string &frag);

	virtual void update(float dt) override;
	virtual void setPosition(const cocos2d::Vec2 &newPosition) override;
	virtual void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags) override;

	virtual void setHeroPosition(const cocos2d::Vec2 &newPosition, bool direction);

protected:
	ShaderNode();
	~ShaderNode();

	bool initWithVertex(const std::string &vert, const std::string &frag);
	void loadShaderVertex(const std::string &vert, const std::string &frag);

	void onDraw(const cocos2d::Mat4& transform, uint32_t flags);

	cocos2d::Vec2 _rect_left_down;//左下
	cocos2d::Vec2 _rect_right_up;//右上

	cocos2d::Vec2 _center;
	cocos2d::Vec2 _resolution;
	float      _time;
	std::string _vertFileName;
	std::string _fragFileName;
	cocos2d::CustomCommand _customCommand;
};


#endif