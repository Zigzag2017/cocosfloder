#include "FireState.h"
#include "Hero.h"
#include "FiniteStateMachine.h"
#include "BaseStage.h"

/**
*开枪状态，可以移动，       方向锁定（按紧攻击，记录方向并锁定） 解除，松开转身
*（马上开枪，立刻后摇）（开枪前摇，发射子弹，开枪后，）     是否在时MOVE，再IDLE
*前摇禁止切状态，除了倒地
*
*注意监听的切断，清楚到发射的动作，到后摇的动作
*可以执行其他动作时，记得解除方向锁定
*
*
*开枪，收枪-> 按紧，没有收枪动作
*/

USING_NS_CC;
void FireState::execute(Hero *hero, MessageEnum messageEnum)
{
	switch (messageEnum)
	{
	case MessageEnum::IDLE:
		if (hero->doFire == false)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	case MessageEnum::MOVE:
		if (hero->doFire == false)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	case MessageEnum::FILL:
		break;
	case MessageEnum::FIRE:
		if (hero->doFire == false && hero->bullets <= 0)
		{
			hero->getFsm()->currentState = MessageEnum::FILL;
			hero->getFsm()->changeState();
			break;
		}
		if (hero->doFire == false)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	case MessageEnum::TUMBLE:
		break;
	case MessageEnum::START:
		break;
	case MessageEnum::SKILL:
		break;
	}
}

void FireState::run(Hero *hero)
{
	hero->currentName = "FireState";

	hero->fire();
	hero->stopActionByTag(100);
	//hero->stopActionByTag(120);
	hero->doFire = true;

	int speed = (hero->direction == true ? 50 : -50);

	if (hero->direction)
		hero->stage->bulletCreator->request()->used(b2Vec2(hero->_body->GetPosition().x + 20 / 32.0, hero->_body->GetPosition().y), speed, hero->stage);//发射子弹
	else
		hero->stage->bulletCreator->request()->used(b2Vec2(hero->_body->GetPosition().x - 20 / 32.0, hero->_body->GetPosition().y), speed, hero->stage);//发射子弹

	hero->bullets -= 10;//用函数



	//发射子弹函数
	auto seq = Sequence::create(DelayTime::create(0.5), CallFuncN::create(CC_CALLBACK_1(FireState::fire, this)), NULL);
	seq->setTag(100);
	hero->runAction(seq);
	//使用tag值
}

void FireState::fire(Node *node)
{
	Hero *hero = (Hero *)node;

	hero->stopActionByTag(100);
	//hero->stopActionByTag(120);

	log("发射。。。。。。。。。。");
	hero->doFire = false;

	if (hero->dirLock)
	if (hero->bullets > 0)
	{
		hero->getFsm()->currentState = MessageEnum::FIRE;
		hero->getFsm()->changeState();
		return;

	}

	if (hero->isMove == true)
	{
		hero->getFsm()->currentState = MessageEnum::MOVE;
		hero->getFsm()->changeState();
	}
	else
	{
		hero->getFsm()->currentState = MessageEnum::IDLE;
		hero->getFsm()->changeState();
	}

	//auto seq = Sequence::create(DelayTime::create(0.5), CallFunc::create([hero](){//记得打断这里
	//	log("收枪。。。。。。。。。。");


	//	//判断武器种类及子弹数。。。。。。。。。。。。。。。。。。。。。。。

	//	if (hero->isMove == true)
	//	{
	//		hero->getFsm()->currentState = MessageEnum::MOVE;
	//		hero->getFsm()->changeState();
	//	}
	//	else
	//	{
	//		hero->getFsm()->currentState = MessageEnum::IDLE;
	//		hero->getFsm()->changeState();
	//	}

	//}), NULL);
	//seq->setTag(120);
	//hero->runAction(seq);
}