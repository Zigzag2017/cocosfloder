#include "SkillState.h"
#include "Hero.h"
#include "FiniteStateMachine.h"
#include "cocos2d.h"
#include "LineBullet.h"

void SkillState::execute(Hero *hero, MessageEnum messageEnum)
{
	switch (messageEnum)
	{
	case MessageEnum::IDLE:
		endSkill(hero);
		//hero->getFsm()->currentState = messageEnum;
		//hero->getFsm()->changeState();
		break;
	case MessageEnum::MOVE:
		endSkill(hero);
		//hero->getFsm()->currentState = messageEnum;
		//hero->getFsm()->changeState();
		break;
	case MessageEnum::FILL:
		break;
	case MessageEnum::FIRE:
		if (hero->bullets <= 0)
		{
			hero->getFsm()->currentState = MessageEnum::FILL;
			hero->getFsm()->changeState();
			break;
		}
		hero->getFsm()->currentState = messageEnum;
		hero->getFsm()->changeState();
		break;
		//case MessageEnum::TUMBLE:
		//	break;
		//case MessageEnum::START:
		//	break;
	case MessageEnum::SKILL:
		if (hero->lines > 0)
		{
			hero->getFsm()->currentState = messageEnum;
			hero->getFsm()->changeState();
		}
		break;
	}
}

void SkillState::run(Hero *hero)
{
	//hero->move();
	hero->currentName = "SkillState";

	//auto seq = Sequence::create(DelayTime::create(100), CallFuncN::create(CC_CALLBACK_1(SkillState::endSkill, this)), NULL);
		//seq->setTag(201);
	hero->skill();
}

void SkillState::endSkill(Node *node)
{
	Hero* hero= (Hero*)node;
	hero->lineBullet->stop();
	hero->dirLock = false;
	if (hero->isMove == true)
	{
		hero->getFsm()->currentState = MessageEnum::MOVE;
		hero->getFsm()->changeState();
	}
	else
	{
		hero->getFsm()->currentState = MessageEnum::IDLE;
		hero->getFsm()->changeState();
	}
}