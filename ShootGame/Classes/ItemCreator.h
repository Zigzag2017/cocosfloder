#ifndef __ITEM_CREATOR_H__
#define __ITEM_CREATOR_H__

#include <stdio.h>
#include "cocos2d.h"
#include "Box2DPhysicsObject.h"
#include "BaseItem.h"

USING_NS_CC;


class ItemCreator
{
public:
	ItemCreator();
	~ItemCreator();
	void addItem(Node* mainView, float x, float y, int type);
};

#endif