#ifndef __BOX2D_MANAGER_H__
#define __BOX2D_MANAGER_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "Box2DContactListener.h"

USING_NS_CC;

class Box2DManager
{
public:
	static Box2DManager* getInstance();
	
	void init();
	void destroy();

	b2World* getWorld();

	static Box2DManager* b2Manager;
	

private:
	
	b2World* _world;
	Box2DContactListener* _contactListener;
	
	Box2DManager()
	{
		_world = nullptr;
		_contactListener = nullptr;
	}
};



#endif