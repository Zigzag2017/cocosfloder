#include "BubbleCreator.h"
#include "BaseBubble.h"
#include "ui/CocosGUI.h"
#include "BaseStage.h"
#include <math.h>
#include "BaseStage.h"

USING_NS_CC;

BubbleCreator::BubbleCreator(int initSize, Node* s)
{
	baseStage = nullptr;
	stage = s;
	m_initSize = initSize;
	allocate(initSize);
}

BubbleCreator::~BubbleCreator()
{


}

BaseBubble* BubbleCreator::request()
{
	if (m_freeList.size() == 0)
	{
		return nullptr;
		//allocate(m_initSize / 2);
	}

	BaseBubble* particle = *m_freeList.begin();

	m_freeList.erase(m_freeList.begin());
	m_usedList.pushBack(particle);

	log("free %d  used %d", m_freeList.size(), m_usedList.size());

	return particle;
}

void BubbleCreator::putback(BaseBubble* particle)
{
	for (int i = 0; i < m_usedList.size(); ++i)
	{
		if (m_usedList.at(i) == particle)
		{
			m_usedList.erase(m_usedList.begin() + i);
			m_freeList.pushBack(particle);
		}
	}
}
void BubbleCreator::setBaseStage(BaseStage* s){ baseStage = s; };

void BubbleCreator::makeParticles(int targetZOrder, float x, float y, int bottom, int xSpeed)
{

	//BaseParticle* p = BaseParticle::create("s1.png", 1);
	BaseBubble* b;
	for (int i = 0; i < 5; i++)
	{
		b = request();

		//Sprite* t = (Sprite*)stage->getChildByTag(110);
		//t->setScale(t->getScale()+1);

		//Sprite* pp = Sprite::create("flower/bubble.png");
		//pp->setScale(20);
		//pp->setPositionX(t->getPositionX());
		//pp->setPositionY(t->getPositionY());
		//pp->setPosition(Vec2(-4 * 480, -7 * 320));
		//stage->addChild(pp);
		if (b != nullptr)
		{
			//p->setPositionX(-1 * 480);
			b->used(this, targetZOrder, x  );
		}
	}

}


/**
*	回收多余长度
*/
void BubbleCreator::garbage()
{
	if (m_freeList.size() > m_initSize)
	{
		Vector<BaseBubble*>::iterator it = m_freeList.begin();
		int pIndex = 0;

		while (it != m_freeList.end())
		{
			if (pIndex > m_initSize)
			{
				BaseBubble* p = (*it);
				m_freeList.erase(it);
				freeParticle(p);
				--it;
			}
			++pIndex;
			++it;
		}
	}
}

void BubbleCreator::allocate(int size)
{
	for (int i = 0; i < 20; ++i)//size%5+1
	{
		BaseBubble* p = (BaseBubble*)stage->getChildByTag(100+i);
		//BaseParticle* p = new BaseParticle(m_style);
		m_freeList.pushBack(p);
	}
}

void BubbleCreator::freeParticle(void *particle)
{
	log(" after delete ");


	BaseBubble* b = (BaseBubble*)particle;
	delete b;
}