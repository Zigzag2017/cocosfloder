#ifndef __GAME_H__
#define __GAME_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "GLES-Render.h"
#include "Box2DContactListener.h"
#include "ArealCreator.h"
#include "EnemyCreator.h"
#include "BulletCreator.h"

class GameScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	virtual void update(float dt);

	// implement the "static create()" method manually
	CREATE_FUNC(GameScene);

	b2World* _world;

	b2Body* _hero;

	ArealCreator* arealCreator;

	EnemyCreator* enemyCreator;

	BulletCreator* bulletCreator;

	//Box2DContactListener* contactListener;

	virtual b2Body* createObjet(float posX, float posY, float radius);
	
	virtual b2Body* createBox(float posX, float posY, float width, float height);

	GLESDebugDraw *_debugDraw;
	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags);

};

#endif // __HELLOWORLD_SCENE_H__