#include "Hero.h"
#include "FiniteStateMachine.h"
#include "LineBullet.h"
#include "BaseStage.h"
#include "GameManager.h"
#include "ui/CocosGUI.h"
#include <math.h>

USING_NS_CC;

using namespace cocostudio;

Hero::Hero() :
bodyAction(nullptr),
handAction(nullptr),
mFsm(nullptr),
direction(true),
canControl(true),
isMove(false),
speed(3)
{
	isFill = false;
	addBullet = false;

	doFire = false;
	dirLock = false;
	direction = true;
	lifes = 100;
	bullets = 100;
	lines = 600;

	fHeight = 0.0;
	fCount = 0.0;

	lockPosition = false;

	isLine = false;
	isBack = false;
	isForward = false;
	objectClass = ObjectClass::HERO_OBJECT;

	lastCos = 0.0;
	currentCos = 0.0;
	lastP = 0.0;
	currentP = 0.0;
}

void Hero::createFixture()
{
	log("Hero createFixture");

	//bodyDef.linearDamping = 1;



	b2FixtureDef fixtureDef;
	fixtureDef.density = 1000;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;

	//fixtureDef.filter.groupIndex = 1;

	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);
}


void Hero::setImage(cocos2d::ui::ImageView* hp, cocos2d::ui::ImageView* mp, cocos2d::ui::ImageView* big_mp)
{
	image_hp = hp;
	image_mp = mp;
	image_big_mp = big_mp;
}


void Hero::createShape()
{
	log("Hero createShape");

	b2CircleShape *shape = new b2CircleShape();
	shape->m_radius = 48.0f / 32.0f;
	_shape = shape;


}

void Hero::createBody()
{
	log("Hero createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(100 / 32.0, 100 / 32.0);
	bodyDef.fixedRotation = true;
	//bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
}


void Hero::handleBeginContact(b2Contact* contact)
{
	//log("contact!!!");
}


bool Hero::init()
{

	if (!Box2DPhysicsObject::init())
	{
		return false;
	}


	auto frameCache = SpriteFrameCache::getInstance();
	auto cache = AnimationCache::getInstance();
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("player/Player.plist");
	//SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Plist.plist");
	SpriteFrame* frame = NULL;
	Vector<SpriteFrame*> frameVec;
	for (int i = 0; i <= 2; i++)
	{
		if (i != 2)
		{
			frame = frameCache->getSpriteFrameByName(StringUtils::format("P/eye/face_%d.png", i));
			frameVec.pushBack(frame);
		}
		else
		{
			frame = frameCache->getSpriteFrameByName(StringUtils::format("P/eye/face.png", i));
			frameVec.pushBack(frame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(frameVec, 0.1, 1);
	cache->addAnimation(animation, "close");

	frameVec.clear();
	for (int i = 1; i <= 5; i++)
	{
		if (i != 5)
		{
			frame = frameCache->getSpriteFrameByName(StringUtils::format("P/eye/hurt.png", i));
			frameVec.pushBack(frame);
		}
		else
		{
			frame = frameCache->getSpriteFrameByName(StringUtils::format("P/eye/face.png", i));
			frameVec.pushBack(frame);
		}
	}
	animation = Animation::createWithSpriteFrames(frameVec, 0.1, 2);
	cache->addAnimation(animation, "eyeHurt");





	shadow = Sprite::createWithSpriteFrameName("P/shadow_player.png");//("shadow.png");//影子
	shadow->setAnchorPoint(Vec2(0.5, 0.5));

	//加载骨骼，上体，下体

	//表情：空闲，敌意，受伤      方块控制
	//状态：受伤，闪烁无敌，

	//上体： 空闲，走路，装弹，开枪，迫击，技能，倒地
	//--空闲 循环动作 动作轻微体现呼吸
	//--走路 循环动作 举枪行走，摆手
	//--装弹 指定动作，体现充能
	//--开枪 举枪发射，有后摇，但是后摇可以被其他动作取代，如连射 锁定方向，可以控制
	//--迫击 举枪，发射，收枪 后摇可以取代 无法控制
	//--技能
	//--倒地 摔倒动作，躺，固定位置，站起来。//无敌，且站起来闪烁无敌一段时间 无法控制

	//（可控制移动的）不循环动作完成后，判断是在移动还是在空闲

	//下体： 前进，空闲，后退   先确定方向，再判断前进后退
	//倒地
	//
	handNode = static_cast<cocostudio::timeline::SkeletonNode *>(CSLoader::createNode("player/Hand.csb"));

	handNode->setScale(0.8f);


	bodyNode = static_cast<cocostudio::timeline::SkeletonNode *>(CSLoader::createNode("player/Body.csb"));

	bodyNode->setScale(0.8f);

	handAction = CSLoader::createTimeline("player/Hand.csb");
	bodyAction = CSLoader::createTimeline("player/Body.csb");

	handNode->runAction(handAction);
	bodyNode->runAction(bodyAction);

	handAction->play("idle", true);
	bodyAction->play("idle", true);

	//action->retain();
	handNode->setPositionX(20);
	handNode->setPositionY(-6);
	this->addChild(handNode);
	this->addChild(bodyNode);


	auto eyeBone = bodyNode->getBoneNode("Body");

	face = eyeBone->getChildByTag(30);

	//this->runAction(Sequence::create(DelayTime::create(2), CallFunc::create([action](){action->play("fire", true); }), NULL));
	//this->runAction(Sequence::create(DelayTime::create(6), CallFunc::create([action](){action->play("fill", true); }), NULL));

	mFsm = FiniteStateMachine::createWithHero(this);
	mFsm->retain();//避免mFsm被自动回收，记得手动回收***************************
	mFsm->currentState = MessageEnum::IDLE;
	mFsm->changeState();//不要在外部change
	log("---------------------------------------------------");

	scheduleUpdate();//启动定时器

	return true;
}

void Hero::update(float d)
{
	updatePosition();
	/*
	if (isLine)
	{
	lines--;
	if (lines <= 0)
	{
	lines = 0;
	if (this->getFsm()->currentState == MessageEnum::SKILL)
	this->getFsm()->execute(MessageEnum::IDLE);

	stage->leftBtn->setEnabled(false);
	}
	}*/
	stage->leftBtn->setEnabled(false);
	if (lifes <= 0)lifes = 0;

	//更新hud信息栏
	image_hp->setScaleX(lifes / 100.0);
	image_mp->setScaleX(bullets / 100.0);
	image_big_mp->setScaleX(0.0 / 600.0);

}

FiniteStateMachine* Hero::getFsm() {
	return mFsm;
}

void Hero::startMove(float a)
{
	moveAngle = a;

	if (canControl)
	{
		isMove = true;

		if (mFsm->currentState == MessageEnum::IDLE)//处于空闲才会切到移动
			mFsm->execute(MessageEnum::MOVE);
		this->_body->SetLinearVelocity(b2Vec2(cos(a) * 10, sin(a) * 10));//10 调配 速度

		float cosValue = cos(a);

		if (dirLock == false && cosValue != 0)
		{
			if (cos(a) * 10 > 0)direction = true;
			else direction = false;
		}

		//log("--------------------**%f**------------%d", cos(a), direction);
		//***************前进后退逻辑
		if (dirLock && (direction == cosValue < 0))
		{

			if (!isBack)
			{
				bodyAction->play("back", true);
				isBack = true;
				isForward = false;
			}
		}
		else
		{
			if (!isForward)
			{
				bodyAction->play("forward", true);
				isForward = true;
				isBack = false;
			}
		}
		//else
		//	bodyAction->play("back", true);
	}
}

void Hero::endMove()//注意不能操控后，记得终止移动速度//走路和空闲无法中断一些状态，也因为是并存
{
	if (isMove)
	{
		isBack = false;
		isForward = false;
		bodyAction->play("idle", true);

		isMove = false;
		if (mFsm->currentState == MessageEnum::MOVE)//处于移动才会切到空闲
			mFsm->execute(MessageEnum::IDLE);
		this->_body->SetLinearVelocity(b2Vec2(0, 0));
	}
}
void Hero::setStage(BaseStage *s)
{
	stage = s;
	s->shadowLayer->addChild(shadow);
}

void Hero::updatePosition()
{
	fCount += 0.04;
	fHeight = 10 * cos(fCount);

	if (addBullet)
	{
		auto bone = handNode->getBoneNode("Bone_1");
		Sprite* s = (Sprite*)bone->getChildByTag(54);
		s->setScaleX(s->getScaleX() + 0.01);
		if (s->getScaleX() >= 1.0)
		{
			addBullet = false;
			isFill = false;
			bullets = 100;
			if (isBack || isForward)
			{
				mFsm->execute(MessageEnum::MOVE);
			}
			else
			{
				mFsm->execute(MessageEnum::IDLE);
			}

		}
	}


	if (canControl&&isMove)
	{
		//this->setPositionX(this->getPositionX() + speed * cos(moveAngle));
		//this->setPositionY(this->getPositionY() + speed * sin(moveAngle));
	}
	 
	 currentCos =  cos(moveAngle);

	 

	if (direction)
	{
		float curent = _body->GetPosition().x * 32.0 + 7 - this->stage->gameManager->currentPoint;
		currentP = curent;
		
		this->setPositionX(curent);
		this->setPositionY(_body->GetPosition().y * 5.536 + 90 + 150 + fHeight);//32.0*0.173（sin10°） = 5.536

		rightCurrentPoint = curent;
		leftCurrentPoint = -1;

		lastP = curent;
	}
	else
	{
		float curent = _body->GetPosition().x * 32.0 - 7 - this->stage->gameManager->currentPoint;
		currentP = curent;
		//if (rightCurrentPoint == -1)
		//{
		//	if (curent < leftCurrentPoint)
		//		this->setPositionX(curent);
		//}
		//else
		{
			this->setPositionX(curent);
		}


		this->setPositionY(_body->GetPosition().y * 5.536 + 90 + 150 + fHeight);

		rightCurrentPoint = -1;
		leftCurrentPoint = this->stage->gameManager->currentPoint;
		lastP = curent;
	}
	this->setZOrder(1000 - (int)_body->GetPosition().y);
	currentCos = cos(moveAngle);
	//if (lockPosition == false)
	shadow->setPositionX(_body->GetPosition().x * 32.0 - this->stage->gameManager->currentPoint);
	shadow->setPositionY(_body->GetPosition().y * 5.536 + 150);
	//shadow->setScaleY(0.3);
	/*
	*是否翻this，待定
	*记得倍数
	*/
	if (direction)
	{
		bodyNode->setScaleX(0.8);
		handNode->setScaleX(0.8);
		handNode->setPositionX(20);
	}
	else
	{
		bodyNode->setScaleX(-0.8);
		handNode->setScaleX(-0.8);
		handNode->setPositionX(-20);
	}


}

void Hero::hurt()
{
	lifes -= 10;

	if (lifes <= 0)
	{
		log("game over");
	}

	isHurt = true;
	auto close = Animate::create(AnimationCache::getInstance()->getAnimation("eyeHurt"));
	auto blink = Blink::create(1, 10);
	auto seq = Sequence::create(blink, CallFuncN::create(CC_CALLBACK_1(Hero::endHurt, this)), NULL);
	face->runAction(close);
	this->runAction(seq);
}

void Hero::endHurt(Node *node)
{
	this->setVisible(true);
	isHurt = false;
}


/////////////////////////////////////////////////   state
void Hero::idle()
{
	log("idle");
	this->handAction->clearLastFrameCallFunc();
	handAction->play("idle", true);
}
void Hero::move()
{
	log("move");
	this->handAction->clearLastFrameCallFunc();
	handAction->play("walk", true);
}
/*void Hero::start()
{
log("start");
//action->play("start", false);
}*/
void Hero::fill()
{

	log("fill");



	if (!isFill){
		handAction->play("fill", false);
		//auto seq = Sequence::create(DelayTime::create(2), CallFuncN::create(CC_CALLBACK_1(Hero::tumble, this)), NULL);
		//seq->setTag(769);
		addBullet = true;
		//this->runAction(seq);
	}

	isFill = true;
}
void Hero::fire()
{
	log("fire");
	this->handAction->clearLastFrameCallFunc();
	handAction->play("fire", false);
}
void Hero::tumble(Ref* r)
{
	isFill = false;
	bullets = 100;
	if (isBack || isForward)
	{
		mFsm->execute(MessageEnum::MOVE);
	}
	else
	{
		mFsm->execute(MessageEnum::IDLE);
	}
	//action->play("tumble", false);
}
void Hero::skill()
{
	log("skill");
	handAction->play("big", false);
	handAction->setLastFrameCallFunc([this](){
		this->handAction->clearLastFrameCallFunc();
		this->lineBullet->start();

	});

	dirLock = true;
}
//////////////////////////////////////////////////////