#include "BaseBullet.h"
#include "ui/CocosGUI.h"
#include <math.h>

USING_NS_CC;



BaseBullet::BaseBullet()
{
	speed = 0;
	

	log("run BaseBullet -> yes");

}

void BaseBullet::createFixture()
{
	log("BaseBullet createFixture");

	//bodyDef.linearDamping = 1;



	b2FixtureDef fixtureDef;
	fixtureDef.density = 100;
	fixtureDef.friction = 0.3;
	fixtureDef.restitution = 0;
	fixtureDef.shape = _shape;
	fixtureDef.isSensor = true;

	//auto object = _world->CreateBody(&bodyDef);
	_body->CreateFixture(&fixtureDef);
}

void BaseBullet::createShape()
{
	log("BaseBullet createShape");

	
	b2PolygonShape *shape = new b2PolygonShape();
	shape->SetAsBox(10 / 2 / 32.0, 6 / 2 / 32.0);
	_shape = shape;


}

void BaseBullet::createBody()
{
	log("BaseBullet createBody");
	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.position.Set(_x / 32.0, _y / 32.0);
	bodyDef.fixedRotation = true;
	//bodyDef.linearDamping = 100;

	_body = _world->CreateBody(&bodyDef);
}

bool BaseBullet::init()
{

	if (!Box2DPhysicsObject::init())
	{
		return false;
	}

	objectClass = ObjectClass::BULLET_OBJECT;

	

	return true;
}