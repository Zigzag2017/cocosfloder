#ifndef __AREAL_CREATOR_H__
#define __AREAL_CREATOR_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "Box2DPhysicsObject.h"

class BaseWall :
	public Box2DPhysicsObject
{
public:

	BaseWall(){};
	virtual bool init(){ return true; };
	//CREATE_FUNC(BaseWall);

	virtual void handleBeginContact(b2Contact* contact){};


//private:


};

class ArealCreator
{
	public:
		ArealCreator(); 
		~ArealCreator();

		Size visibleSize;

		void init();
		b2Body* createBox(float posX, float posY, float width, float height, int type);
		void updatePosition(float heroDistance);

	private:
		b2Body* topWall;
		b2Body* bottomWall;
		b2Body* leftInsulateWall;
		b2Body* rightInsulateWall;
		b2Body* leftWall;
		b2Body* rightWall;
		b2World* _world;

		BaseWall* topWallView;
		BaseWall* bottomWallView;
		BaseWall* leftInsulateWallView;
		BaseWall* rightInsulateWallView;
		BaseWall* leftWallView;
		BaseWall* rightWallView;
};

#endif