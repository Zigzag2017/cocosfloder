#include "cocos2d.h"
#include "Boss.h"
#include "BaseEnemy.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <math.h>
#include "BaseStage.h"
#include "Hero.h"
#include "GameManager.h"
#include "LineBullet.h"
#include "Box2DPhysicsObject.h"

USING_NS_CC;



Boss::Boss(float x, float y, Node* h)
{
	_x = x;
	_y = y;
	hero = (Hero*)h;
	lifes = 180;
	speed = 10000;
	isMove = false;
	isHurt = false;
	isAttack = false;
	moveAnimation = nullptr;
	attackAnimation = nullptr;
	hurtAnimation = nullptr;

	fHeight = 0.0;
	fCount = 0.0;

	isShock = false;

	isCatch = false;

	disColor = Vec4(0.0, 0.0, 0.0, 0.0);



}

void Boss::createShape()
{
	log("BaseEnemy createShape");

	b2CircleShape *shape = new b2CircleShape();
	shape->m_radius = 48.0f / 32.0f;
	_shape = shape;

	b2PolygonShape *rectShape = new b2PolygonShape();
	b2Vec2 v[4];
	v[0].Set(96 / 32.0, -96 / 32.0);
	v[1].Set(96 / 32.0, 96 / 32.0);
	v[2].Set(0, 96 / 32.0);
	v[3].Set(0, -96 / 32.0);
	rectShape->Set(v, 4);
	//rectShape->SetAsBox(48 / 2 / 32.0, 96 / 2 / 32.0);
	_hitShape = rectShape;


}




void Boss::handleBeginContact(b2Contact* contact)
{
	//log("contact!!!");
	Box2DPhysicsObject* other;
	b2Fixture* self;
	if (this == contact->GetFixtureA()->GetBody()->GetUserData())
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureA()->GetUserData();
	}
	else
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureB()->GetUserData();
	}

	if (!isCatch&&!self&&other->objectClass == ObjectClass::HERO_OBJECT)
	{
		isCatch = true;
		log("catch..........");
		if (this->isHurt == false && this->isDeath == false)
			this->attack();
	}

	if (other->objectClass == ObjectClass::BULLET_OBJECT){
		if (((MainBullet*)other)->isActive&&this->isHurt == false && this->isDeath == false)//子弹处于isActive为true才会受伤
		{
			((MainBullet*)other)->isActive = false;
			if (((MainBullet*)other)->speed > 0)//子弹速度方向，被攻击后面朝反方向
				direction = false;
			else
				direction = true;
			//this->runAction(Blink::create(0.2, 5));
			this->hurt();
		}
	}
	//log("begin...................");
}

void Boss::handleEndContact(b2Contact* contact)
{

	//**********************捕获敌人，需要重构

	//log("end...................");
	//log("contact!!!");
	Box2DPhysicsObject* other;
	b2Fixture* self;
	if (this == contact->GetFixtureA()->GetBody()->GetUserData())
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureA()->GetUserData();
	}
	else
	{
		other = (Box2DPhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData();
		self = (b2Fixture*)contact->GetFixtureB()->GetUserData();
	}

	if (!self)
	{
		isCatch = false;
		log("no catch.............");
	}

	if (other->objectClass == ObjectClass::LINE_OBJECT)
	{
		log("remove line~~~~~~~~~~~~~~~~~~~~~~~");
		if (hero->lineBullet->m_catchList.getIndex(this) != -1)
		{
			hero->lineBullet->m_catchList.erase(hero->lineBullet->m_catchList.getIndex(this));
			endShock();
		}

	}

}

void Boss::shock()
{
	isShock = true;
	//if (!isDeath)
	//	bodyAction->play("hurt", true);
}
void Boss::endShock()
{
	isShock = false;
	//if (!isDeath)
	//	bodyAction->play("idle", true);
}



bool Boss::init()
{

	if (!BaseEnemy::init())
	{
		return false;
	}

	//isMove = true;


	auto cache = AnimationCache::getInstance();
	auto frameCache = SpriteFrameCache::getInstance();
	SpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("player/Boss.plist");

	//bodyNode = static_cast<cocostudio::timeline::SkeletonNode *>(CSLoader::createNode("player/Sailor.csb"));

	////bodyNode->setScale(0.8f);

	//bodyAction = CSLoader::createTimeline("player/Sailor.csb");


	//bodyNode->runAction(bodyAction);


	//bodyAction->play("idle", true);


	//this->addChild(bodyNode);

	frameCache->addSpriteFramesWithFile("ePlist.plist", "ePlist.png");
	SpriteFrame* frame = NULL;
	Vector<SpriteFrame*> frameVec;
	
		frame = frameCache->getSpriteFrameByName("Boss/attack0001.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0003.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0005.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0007.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0009.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0011.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0013.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0015.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0017.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0019.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0021.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0023.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0025.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0027.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0029.png");
		frameVec.pushBack(frame);
	/*	frame = frameCache->getSpriteFrameByName("Boss/attack0031.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0033.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0035.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/attack0037.png");
		frameVec.pushBack(frame);*/


	
	auto attackAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, 1);
	cache->addAnimation(attackAnimation, "attack");

	//frameVec.clear();
	//for (int i = 1; i <= 3; i++)
	//{
	//	frame = frameCache->getSpriteFrameByName(StringUtils::format("animation/enemy_hurt000%d.png", i));
	//	//if (!frame)log("********************null**************************");
	//	frameVec.pushBack(frame);
	//}
	//auto hurtAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, 1);
	//cache->addAnimation(hurtAnimation, "hurt");

	frameVec.clear();
	
		frame = frameCache->getSpriteFrameByName("Boss/walk0001.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0003.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0005.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0007.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0009.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0011.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0013.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0015.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0017.png");
		frameVec.pushBack(frame);
		frame = frameCache->getSpriteFrameByName("Boss/walk0019.png");
		frameVec.pushBack(frame);
	
	auto moveAnimation = Animation::createWithSpriteFrames(frameVec, 0.1, -1);
	cache->addAnimation(moveAnimation, "move");

	auto an = Animate::create(moveAnimation);
	an->setTag(222);
	this->runAction(an);

	//this->move();

	shadow = Sprite::createWithSpriteFrameName("P/shadow_player.png");
	shadow->setAnchorPoint(Vec2(0.5, 0.5));
	//body = Sprite::create("enemy.png");
	//this->addChild(body);
	_beginContactCallEnabled = true;
	_endContactCallEnabled = true;

	scheduleUpdate();//启动定时器

	return true;
}

void Boss::update(float d)
{

	auto s = Director::getInstance()->getWinSize();

	fCount += 0.04;
	fHeight = 10 * cos(fCount);

	//m_eGLProgramState->setUniformVec2("resolution", Vec2(s.width, s.height));

	//m_eGLProgramState->setUniformVec4("v_disColor", disColor);

	//敌人状态机
	b2Vec2 position = _body->GetPosition();
	b2Vec2 heroPosition = hero->_body->GetPosition();


	



	shadow->setPositionX(_body->GetPosition().x * 32.0 - this->stage->gameManager->currentPoint);
	shadow->setPositionY(_body->GetPosition().y * 5.536 + 150);
	//shadow->setVisible(false);
	//shadow->setScaleY(0.3);

	this->setZOrder(1000 - (int)position.y);

	if (isShock)
	{
		this->lifes -= 1;
		if (this->lifes <= 0)
		{
			death();
			isShock = false;
		}
	}

	b2Vec2 v;
	if (position.x - heroPosition.x > 0)
	{
		v.x = -1 * speed;
		direction = true;
		this->setScaleX(-1.2);
		this->setScaleY(1.2);
		_body->SetTransform(position, 3.14);

	}
	else
	{
		v.x = speed;
		direction = false;
		this->setScaleX(1.2);
		this->setScaleY(1.2);
		_body->SetTransform(position, 0);

	}

	if (isHurt || isDeath){
		if (this->getScaleX()<0)
		{
			this->setPositionX(position.x * 32.0 - 90. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 70);
		}
		else
		{
			this->setPositionX(position.x * 32.0 + 90. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 70);
		}
	}
	else if (isAttack){
		if (this->getScaleX()<0)
		{
			this->setPositionX(position.x * 32.0 - 85. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 130);
		}
		else
		{
			this->setPositionX(position.x * 32.0 + 85. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 130);
		}
	}
	else{
		this->move();
		if (this->getScaleX()<0)
		{
			this->setPositionX(position.x * 32.0 - 90. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 70);
		}
		else
		{
			this->setPositionX(position.x * 32.0 + 90. - this->stage->gameManager->currentPoint);
			this->setPositionY(position.y * 5.536 + 80 + 150 + fHeight + 70);
		}

		
		
		if (position.y - heroPosition.y > 0)
		{
			v.y = -0.3 * speed;
		}
		else
		{
			v.y = 0.3 * speed;
		}

		//_body->SetLinearVelocity(v);
		_body->ApplyLinearImpulse(v, _body->GetWorldCenter(), true);

	}

}


void Boss::idle()
{

}

void Boss::move()
{
	if (!isMove&&!this->isAttack)
	{
		//bodyAction->clearLastFrameCallFunc();
		this->stopActionByTag(222);

		isMove = true;
		moveAnimation = AnimationCache::getInstance()->getAnimation("move");
		
		auto action = Animate::create(moveAnimation);
			//auto action = RepeatForever::create(a);
		action->setTag(222);//同一个状态机
		this->runAction(action);
	}
}
void Boss::attack()
{
	if (!this->isAttack)
	{
		
		this->stopActionByTag(222);
		this->isAttack = true;
		isHurt = false;
		isMove = false;



		attackAnimation = AnimationCache::getInstance()->getAnimation("attack");
		auto a = Animate::create(attackAnimation);
		
		auto seq = Sequence::create(a, CallFunc::create(CC_CALLBACK_0(Boss::endAttack, this)), NULL);
		seq->setTag(222);//同一个状态机
		this->runAction(seq);


		//seq->setTag(100);//受伤无法被中断，但是攻击会，给攻击用
		//this->runAction(seq);
	}
}
void Boss::checkAttack()
{
	if (isCatch == true)
	{
		hero->hurt();
	}
}

void Boss::endAttack()
{

	//this->bodyAction->clearLastFrameCallFunc();

	//没有攻击帧的简单判断
	checkAttack();
	isMove = false;
	this->isAttack = false;
	//isMove = true;
	if (isCatch == true)
	if (this->isHurt == false && this->isDeath == false)
	{
		this->attack();
	}
}

void Boss::hurt(){
	if (!this->isHurt&&!this->isDeath)
	{
		//bodyAction->clearLastFrameCallFunc();
		//根据受伤方向，确定火星溅射方向
		if (!this->direction)
			stage->particleCreator->makeParticles(this->getZOrder(), shadow->getPositionX(), shadow->getPositionY() + 100, shadow->getPositionY() + 30, 1);
		else
			stage->particleCreator->makeParticles(this->getZOrder(), shadow->getPositionX(), shadow->getPositionY() + 100, shadow->getPositionY() + 30, -1);

		this->lifes -= 350;//扣血，确定子弹，扣多少//************

		this->stopActionByTag(222);//受伤停止攻击
		this->isAttack = false;
		this->isHurt = true;
		isMove = false;
		//hurtAnimation = AnimationCache::getInstance()->getAnimation("hurt");
		//auto a = Animate::create(hurtAnimation);
		//bodyAction->play("hurt", false);
		auto seq = Sequence::create(DelayTime::create(0.2), CallFunc::create(CC_CALLBACK_0(Boss::endHurt, this)), NULL);
		seq->setTag(222);//同一个状态机
		this->runAction(seq);

		//this->setColor(Color3B::RED);

	}
}
void Boss::endHurt(){
	//bodyAction->clearLastFrameCallFunc();
	//this->setColor(Color3B::WHITE);

	//if (this->lifes < 0)
	//{
	//	death();
	//}
	this->isHurt = false;
	//this->isMove = true;
	//this->setColor(Color3B::);
}

void Boss::death()
{

	//bodyAction->clearLastFrameCallFunc();

	this->isDeath = true;

	if (hero->lineBullet->m_catchList.getIndex(this) != -1)
	{
		hero->lineBullet->m_catchList.erase(hero->lineBullet->m_catchList.getIndex(this));
	}

	//this->setColor(Color3B::RED);
	//hurtAnimation = AnimationCache::getInstance()->getAnimation("hurt");
	//auto a = Animate::create(hurtAnimation);
	//bodyAction->play("explosion", false);

	//bodyAction->setLastFrameCallFunc(
	//	CC_CALLBACK_0(Boss::endDeath, this)
	//	);

	auto seq = Sequence::create(DelayTime::create(1), CallFunc::create(CC_CALLBACK_0(Boss::endDeath, this)), NULL);
	this->runAction(seq);
}

void Boss::endDeath()
{

	//bodyAction->clearLastFrameCallFunc();

	stage->killNum++;

	stage->shadowLayer->removeChild(shadow);
	stage->objectLayer->removeChild(this);

}

void Boss::turning(){}

void Boss::setStage(BaseStage *s)
{
	stage = s;
	s->shadowLayer->addChild(shadow);
}