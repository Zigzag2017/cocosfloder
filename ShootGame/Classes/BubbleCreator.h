#ifndef __BUBBLE_CREATOR_H__
#define __BUBBLE_CREATOR_H__


#include "cocos2d.h"

using namespace cocos2d;
class BaseBubble;
class BaseStage;
//class BaseStage;
class BubbleCreator
{
public:
	BubbleCreator(int initSize, Node* s);
	~BubbleCreator();
	BaseBubble* request();
	void putback(BaseBubble* particle);
	void garbage();

	void makeParticles(int targetZOrder, float x, float y, int bottom, int xSpeed);
	Node* stage;
	BaseStage* baseStage;
	void setBaseStage(BaseStage* s);
	
	


private:
	void allocate(int size);
	static void freeParticle(void *particle);
	Vector<BaseBubble*>m_freeList;
	Vector<BaseBubble*>m_usedList;

	int m_initSize;
	int m_style;

};



#endif