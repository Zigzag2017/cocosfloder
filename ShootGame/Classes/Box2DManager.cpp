#include "Box2DManager.h"
#include "Box2D\Box2D.h"
#include "Box2DContactListener.h"

Box2DManager* Box2DManager::b2Manager = nullptr;

Box2DManager* Box2DManager::getInstance()
{
	if (!b2Manager)
	{
		b2Manager = new Box2DManager();
		//s_SharedDirector->init();
	}

	return b2Manager;
}

void Box2DManager::init()
{
	_world = new b2World(b2Vec2(0, 0));

	_contactListener = new Box2DContactListener();//�ǵ�ɾ��
	_world->SetContactListener(_contactListener);
}

b2World* Box2DManager::getWorld()
{
	return _world;
}

void Box2DManager::destroy()
{
	CC_SAFE_DELETE(_world);
	CC_SAFE_DELETE(_contactListener);
	_world = nullptr;
	_contactListener = nullptr;
}

/*
Box2DManager::Box2DManager()
{
	_world = nullptr;
	_contactListener = nullptr;
}*/