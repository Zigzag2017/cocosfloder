#ifndef __BASE_ITEM_H__
#define __BASE_ITEM_H__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Box2DPhysicsObject.h"


USING_NS_CC;

class BaseStage;

class BaseItem :
	public Box2DPhysicsObject
{
public:
	BaseItem(float x, float y, int type);
	virtual bool init(int  type);
	~BaseItem();

	static BaseItem* create(float x, float y,int  type)
	{
		BaseItem *pRet = new BaseItem(x,y,type);
		if (pRet && pRet->init(type))
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = nullptr;
			return nullptr;
		}
	};
	bool btn;
	int type;

	float fHeight;//浮动 高度
	float fCount;

	virtual void createFixture();
	virtual void createShape();
	virtual void createBody();

	virtual void handleBeginContact(b2Contact* contact);
	virtual void handleEndContact(b2Contact* contact);

	bool doDeath;
	bool isDeath;
	void death();

	BaseStage* stage;
	void setStage(BaseStage* s);

	Sprite* shadow;

private:

	virtual void update(float d);

	int identifier;//唯一标识符

	float moveAngle;//移动角度





};


#endif