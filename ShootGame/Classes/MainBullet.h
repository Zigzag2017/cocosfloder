#ifndef __MAIN_BULLET_H__
#define __MAIN_BULLET_H__

#include "cocos2d.h"
#include "BaseBullet.h"

class BaseStage;

class MainBullet :
	public BaseBullet
{
public:

	MainBullet(int style);
	virtual bool init();
	//CREATE_FUNC(MainBullet);
	/*
	static MainBullet* create(float x, float y)
	{
		MainBullet *pRet = new MainBullet(style);
		if (pRet && pRet->init())
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = nullptr;
			return nullptr;
		}
	};*/
	BaseStage* stage;

	float heroHeight;

	virtual void createFixture();
	virtual void createShape();
	virtual void createBody();

	virtual void handleBeginContact(b2Contact* contact);

	/////////////////״̬
	virtual void idle(){};//����

	virtual void explode(){};//��ը

	//only
	virtual void used(b2Vec2 vec,float speed,Node* stage);
	virtual void putback();



private:

	virtual void update(float d);

	//int identifier;//Ψһ��ʶ��
	//int speed;//�ƶ��ٶ�
	//float moveAngle;//�ƶ��Ƕ�

	int m_style;


};


#endif