uniform sampler2D u_texture;       //模型自身纹理
//uniform sampler2D u_lightTexture;   //波光纹理
//uniform vec4 v_LightColor;					//波光的颜色
//uniform vec2 v_animLight;     			//UV变动值
varying vec2 v_texCoord;


//片段着色程序
void main(void) 
{
    //vec4 lightcolor = texture2D(u_lightTexture, v_texCoord + v_animLight.xy) * v_LightColor;
    //和波光纹理混合
	gl_FragColor = texture2D(u_texture, v_texCoord) ;//+ lightcolor;
}

/*
//*水波晃动

// get wave height based on distance-to-center
float waveHeight(vec2 p) {
    float timeFactor = 3.0;
	float texFactor = 12.0;
	float ampFactor = 0.01;
    float dist = length(p);
    return cos(CC_Time.y * timeFactor + dist * texFactor) * ampFactor;
}

//片段着色程序
void main(void) 
{
/*
if(abs(v_texCoord.x-0.5)<0.05)//对接两边，不要动太大
{
	vec4 lightcolor = texture2D(u_lightTexture, v_texCoord + v_animLight.xy) * v_LightColor;
    //和波光纹理混合
	gl_FragColor = texture2D(u_texture, v_texCoord) + lightcolor;
	return;
}*//*
	  // convert to [-1, 1]
    vec2 p = -1.0 + 2.0 * v_texCoord;
    vec2 normal = normalize(p);
	// offset texcoord along dist direction
	vec2 v_texLight = v_texCoord + normal * waveHeight(p) * (1 - 2.0*abs(v_texCoord.x-0.5));
	
    vec4 texLight = texture2D(u_lightTexture, v_texLight + v_animLight.xy) * v_LightColor;
	texLight.w = 0.6;
	//和波光纹理混合
	gl_FragColor = texture2D(u_texture, v_texCoord) + texLight;

}

*/