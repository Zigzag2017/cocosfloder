// Shader from http://www.iquilezles.org/apps/shadertoy/

#ifdef GL_ES
precision highp float;
#endif

uniform vec2 center;
uniform vec2 resolution;

uniform float tt;

uniform sampler2D u_sea;   




vec4 color(vec2 uv)
{
    if (uv.x >= 0.0 && uv.x <= 1.0 && uv.y >=0.0 && uv.y <=1.0)
    {
      return vec4(texture2D( u_sea, uv ).xyz, 1.0);
    }
    return vec4(0.0,0.0,0.0,0.0);
}

void main(void)
{


	float iGlobalTime = CC_Time[1];
	vec2 iResolution = resolution;

	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	uv.y = 1.0 - uv.y; 

    //vec2 target = vec2(680.0,320.0) /  iResolution.xy;
    
   // vec2 pos = uv + normalize(uv - target) * tt;//(sin(iGlobalTime)+1.0)*0.5;
    
	gl_FragColor = vec4(texture2D( u_sea, uv ).xyz, 1.0)*vec4(0.3,0.3,0.3,1.0);
}
/*
void main(void)
{
	
	
	iGlobalTime *= 0.5; 

	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	uv.y = 1.0 - uv.y; 
	//uv.x = 1.0 - uv.x; 
	float bright = 
	- sin(uv.y * slope1 + uv.x * 30.0+ iGlobalTime *3.10) *.2 
	- sin(uv.y * slope2 + uv.x * 37.0 + iGlobalTime *3.10) *.1
	- cos(              + uv.x * 2.0 * slopeSign + iGlobalTime *2.10) *.1 
	- sin(              - uv.x * 5.0 * slopeSign + iGlobalTime * 2.0) * .3;
	
	float modulate = abs(cos(iGlobalTime*.1) *.5 + sin(iGlobalTime * .7)) *.5;
	bright *= modulate;
	vec4 pix = texture2D(u_sea,uv);
	pix.rgb += clamp(bright / 1.0,0.0,1.0);
	gl_FragColor = pix;
}
*/