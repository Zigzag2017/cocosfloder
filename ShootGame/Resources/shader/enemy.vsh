attribute vec3 a_position;
attribute vec2 a_texCoord;

//纹理坐标
varying vec2 v_texCoord;



//顶点着色程序
void main()
{
    gl_Position  = CC_PMatrix * vec4(a_position, 1.0);


    v_texCoord   = a_texCoord;

}
