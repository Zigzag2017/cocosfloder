
//uniform vec4 v_LightColor;					//波光的颜色
uniform float v_dH;
varying vec2 v_texCoord;


//*
/*
vec2 hash2(vec2 p ) {
   return vec2(0.0,0.0);// vec2(     fract(sin(dot(p, vec2(123.4, 748.6))))*5232.85324, fract(sin(dot(p, vec2(547.3, 659.3))))*5232.85324     );   
}
float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);   
}*/


vec2 hash2(vec2 p ) {
   return fract(sin(vec2(dot(p, vec2(123.4, 748.6)), dot(p, vec2(547.3, 659.3))))*5232.85324);   
}
float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);   
}


//Based off of iq's described here: http://www.iquilezles.org/www/articles/voronoilin
float voronoi(vec2 p) {

	float iGlobalTime = CC_Time[1];

    vec2 n = floor(p);
    vec2 f = fract(p);
    float md = 5.0;
    vec2 m = vec2(0.0);
    for (int i = -1;i<=1;i++) {
        for (int j = -1;j<=1;j++) {
            vec2 g = vec2(i, j);
            vec2 o = hash2(n+g);
            o = 0.5+0.5*sin(iGlobalTime+5.038*o);
            vec2 r = g + o - f;
            float d = dot(r, r);
            if (d<md) {
              md = d;
              m = n+g+o;
            }
        }
    }
    return md;
}

float ov(vec2 p) {
	


    float v = 0.0;
    float a = 0.4;
    for (int i = 0;i<3;i++) {
        v+= voronoi(p)*a;
        p*=2.0;
        a*=0.5;
    }
    return v;
}




//片段着色程序
void main(void) 
{


	float iGlobalTime = CC_Time[1];
	vec2 iResolution = vec2(960.0,640.0);//resolution;

	

            
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
    vec4 a = vec4(0.2, 0.2, 0.2, 1.0);
    vec4 b = vec4(0.85, 0.9, 1.0, 1.0);
	gl_FragColor = vec4(mix(a, b, smoothstep(0.0, 0.5, ov(uv*5.0))));
	gl_FragColor *= gl_FragColor;

	vec4 uu = texture2D(CC_Texture0, v_texCoord ) ;//* vec4(0.0,0.0,0.0,1.0);

	if(uu.w <= 0.5)
		gl_FragColor= vec4(0.0,0.0,0.0,0.0)  ;
	else if(uu.x< 0.3||uu.y< 0.3||uu.z < 0.3)
		gl_FragColor= gl_FragColor + texture2D(CC_Texture0, v_texCoord )  ;
	else
		gl_FragColor= texture2D(CC_Texture0, v_texCoord ) ;



	//gl_FragColor= texture2D(CC_Texture0, v_texCoord ) * vec4(1.0,0.0,0.0,1.0) ;

	//gl_FragColor = ;
}
