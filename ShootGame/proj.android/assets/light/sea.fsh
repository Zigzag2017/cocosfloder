// Shader from http://www.iquilezles.org/apps/shadertoy/

#ifdef GL_ES
precision highp float;
#endif

uniform vec2 center;
uniform vec2 resolution;


uniform sampler2D u_sea;   //��ˮ

const bool leftToRight = true;
float slopeSign = (leftToRight ? -1.0 : 1.0);
float slope1 = 5.0 * slopeSign;
float slope2 = 7.0 * slopeSign;

void main(void)
{
	float iGlobalTime = CC_Time[1];
	vec2 iResolution = resolution;
	
	iGlobalTime *= 0.5; 

	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	uv.y = 1.0 - uv.y; 
	uv.x = 1.0 - uv.x; 
	float bright = 
	- sin(uv.y * slope1 + uv.x * 30.0+ iGlobalTime *3.10) *.2 
	- sin(uv.y * slope2 + uv.x * 37.0 + iGlobalTime *3.10) *.1
	- cos(              + uv.x * 2.0 * slopeSign + iGlobalTime *2.10) *.1 
	- sin(              - uv.x * 5.0 * slopeSign + iGlobalTime * 2.0) * .3;
	
	float modulate = abs(cos(iGlobalTime*.1) *.5 + sin(iGlobalTime * .7)) *.5;
	bright *= modulate;
	vec4 pix = texture2D(u_sea,uv);
	pix.rgb += clamp(bright / 1.0,0.0,1.0);
	gl_FragColor = pix;
}
