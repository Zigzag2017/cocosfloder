LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp \
                   ../../Classes/ArealCreator.cpp \
				   ../../Classes/BackgroundManager.cpp \
				   ../../Classes/BaseBubble.cpp \
                   ../../Classes/BaseBullet.cpp \
                   ../../Classes/BaseEnemy.cpp \
                   ../../Classes/BaseItem.cpp \
                   ../../Classes/BaseParticle.cpp \
                   ../../Classes/BaseStage.cpp \
                   ../../Classes/Box2DContactListener.cpp \
                   ../../Classes/Box2DManager.cpp \
                   ../../Classes/Box2DPhysicsObject.cpp \
				   ../../Classes/Boss.cpp \
				   ../../Classes/DarkLayer.cpp \
				   ../../Classes/BubbleCreator.cpp  \
                   ../../Classes/BulletCreator.cpp  \
                   ../../Classes/EnemyCreator.cpp \
                   ../../Classes/FillState.cpp \
                   ../../Classes/FiniteStateMachine.cpp \
                   ../../Classes/FireState.cpp \
                   ../../Classes/Game.cpp \
                   ../../Classes/GameManager.cpp \
                   ../../Classes/GLES-Render.cpp \
                   ../../Classes/Hero.cpp \
                   ../../Classes/IdleState.cpp \
                   ../../Classes/ItemCreator.cpp  \
				   ../../Classes/LightLayer.cpp \
				   ../../Classes/LineBullet.cpp \
                   ../../Classes/MainBullet.cpp \
                   ../../Classes/MoveState.cpp \
                   ../../Classes/NormalEnemy.cpp \
                   ../../Classes/ParticleCreator.cpp \
                   ../../Classes/SelectScene.cpp \
                   ../../Classes/ShadowLayer.cpp \
                   ../../Classes/SkillState.cpp \
                   ../../Classes/VirtualHandle.cpp \
                   ../../Classes/Welcome.cpp \
                   ../../Classes/WelcomeReader.cpp \
                   ../../Classes/WelcomeScene.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
